<h3>Nutzen Sie bequem unser Kontaktformular</h3>

<section class="content kontaktformular">
	<form id="contact">
		<span class="input input--hoshi">
			<input class="input__field input__field--hoshi" type="text" id="input-4" name="name"/>
			<label class="input__label input__label--hoshi input__label--hoshi-color-1" for="input-4">
				<span class="input__label-content input__label-content--hoshi">Name</span>
			</label>
		</span>
		<span class="input input--hoshi">
			<input class="input__field input__field--hoshi" type="text" id="input-5" name="email"/>
			<label class="input__label input__label--hoshi input__label--hoshi-color-2" for="input-5">
				<span class="input__label-content input__label-content--hoshi">E-Mail</span>
			</label>
		</span>
		<span class="input input--hoshi">
			<!--<input class="input__field input__field--hoshi" type="text" id="input-6" />-->
			<label class="input__label input__label--hoshi input__label--hoshi-color-3 noborder" for="input-6">
				<span class="input__label-content input__label-content--hoshi noborder">Ihre Nachricht</span>
			</label>
			<textarea id="input-6" name="message"></textarea>
		</span>
	</form>
</section>
<a id="contactsend" class="button seemore">Senden</a>

<script language="javascript" type="text/javascript" src="/wp-content/themes/skriptstube-theme/kontakt/js/classie.js"></script>
<script>
	(function() {
		// trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
		if (!String.prototype.trim) {
			(function() {
				// Make sure we trim BOM and NBSP
				var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
				String.prototype.trim = function() {
					return this.replace(rtrim, '');
				};
			})();
		}

		[].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
			// in case the input is already filled..
			if( inputEl.value.trim() !== '' ) {
				classie.add( inputEl.parentNode, 'input--filled' );
			}

			// events:
			inputEl.addEventListener( 'focus', onInputFocus );
			inputEl.addEventListener( 'blur', onInputBlur );
		} );

		function onInputFocus( ev ) {
			classie.add( ev.target.parentNode, 'input--filled' );
		}

		function onInputBlur( ev ) {
			if( ev.target.value.trim() === '' ) {
				classie.remove( ev.target.parentNode, 'input--filled' );
			}
		}
	})();
	

	$( document ).ready(function() {
		if($('.kontaktformular textarea').val().length > 0){
			$(".kontaktformular textarea").addClass("hasinput");
		}
		
		 $('.kontaktformular textarea').keyup(function(){
			if($(this).val().length > 0)
			{
				$(this).addClass("hasinput");
			}
			else
			{
				$(this).removeClass("hasinput");
			}
		});
	});
	
	$("#contactsend").click(function(){
	$.ajax({
		type: "POST",
		url: '/wp-content/themes/skriptstube-theme/kontakt/save.php',
		data: {
			name: $(".kontaktformular [name=name]").val(),
			email: $(".kontaktformular [name=email]").val(),
			message: $(".kontaktformular [name=message]").val()
			},
		success: function(data) {
			if (data.success)
			{
				alert("Vielen Dank für Ihre Kontaktanfrage!");
				$(".subfooter .first input[type='text']").val("");
				$(".subfooter .first textarea").val("");
				
				
			}
			else
			{
				var message = "";
				
				for (var i = 0; i < data.message.length; i++)
				{
					message += data.message[i] + "\n";
				}
				
				alert(message);
			}
		}
	});
});
	
</script>


<style type="text/css">
section.kontaktformular{
	margin-top:50px;
}

.kontaktformular textarea{
	border: 1px solid #B9C1CA;
	margin-top:60px;
	height:200px;
	width:100%;
	max-width:800px;
	padding:10px;
}

.kontaktformular textarea:focus,
.kontaktformular textarea.hasinput{
	border: 3px solid hsl(200, 100%, 50%);
	outline: none !important;
    box-shadow: none;
}

.input {
	position: relative;
	z-index: 1;
	display: inline-block;
	margin: 1em 0px;
	max-width: 100%;
	width: calc(100% - 2em);
	vertical-align: top;
}

.input__field {
	position: relative;
	display: block;
	float: left;
	padding: 0.8em;
	width: 100%;
	max-width: 350px;
	border: none;
	border-radius: 0;
	background: #f0f0f0;
	color: #777777;
	/*font-weight: bold;*/
	/*font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;*/
	-webkit-appearance: none; /* for box shadows to show on iOS */
}

.input__field:focus {
	outline: none;
}

.input__label {
	display: inline-block;
	float: right;
	padding: 0 1em;
	width: 40%;
	color: #777777;
	font-weight: bold;
	/*font-size: 70.25%;*/
	-webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

.input__label-content {
	position: relative;
	display: block;
	padding: 1.6em 0;
	width: 100%;
}

.graphic {
	position: absolute;
	top: 0;
	left: 0;
	fill: none;
}

.icon {
	color: #ddd;
	font-size: 150%;
}


/* Hoshi */
.input--hoshi {
	overflow: hidden;
	display:block;
}

.input__field--hoshi {
	margin-top: 1em;
	padding: 0.85em 0.15em;
	width: 100%;
	background: transparent;
	color: #777777;
	padding-left:10px;
}

.input__label--hoshi {
	position: absolute;
	bottom: 0;
	left: 0;
	padding: 0 0.25em;
	width: 100%;
	height: calc(100% - 1em);
	text-align: left;
	pointer-events: none;
}

.input__label-content--hoshi {
	position: absolute;
	margin-top: -20px;
	height:30px;
}

.input__label--hoshi::before,
.input__label--hoshi::after {
	content: '';
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	max-width: 350px;
	height: calc(100% - 10px);
	border-bottom: 1px solid #B9C1CA;
}
.input__label--hoshi.noborder::before,
.input__label--hoshi.noborder::after{
	border-bottom:0px;
}

.input__label--hoshi::after {
	margin-top: 2px;
	border-bottom: 4px solid red;
	-webkit-transform: translate3d(-100%, 0, 0);
	transform: translate3d(-100%, 0, 0);
	-webkit-transition: -webkit-transform 0.3s;
	transition: transform 0.3s;
}

.input__label--hoshi-color-1::after {
	border-color: hsl(200, 100%, 50%);
}

.input__label--hoshi-color-2::after {
	border-color: hsl(200, 100%, 50%);/*hsl(160, 100%, 50%);*/
}

.input__label--hoshi-color-3::after {
	border-color: hsl(200, 100%, 50%);/*hsl(20, 100%, 50%);*/
}

.input__field--hoshi:focus + .input__label--hoshi::after,
.input--filled .input__label--hoshi::after {
	-webkit-transform: translate3d(0, 0, 0);
	transform: translate3d(0, 0, 0);
}

.input__field--hoshi:focus + .input__label--hoshi .input__label-content--hoshi,
.input--filled .input__label-content--hoshi {
	-webkit-animation: anim-1 0.3s forwards;
	animation: anim-1 0.3s forwards;
}

@-webkit-keyframes anim-1 {
	50% {
		opacity: 0;
		/*
		-webkit-transform: translate3d(1em, 0, 0);
		transform: translate3d(1em, 0, 0);
		*/
	}
	51% {
		opacity: 0;
		/*
		-webkit-transform: translate3d(-1em, -40%, 0);
		transform: translate3d(-1em, -40%, 0);
		*/
		margin-top:-20px;
	}
	100% {
		opacity: 1;
		/*
		-webkit-transform: translate3d(0, -40%, 0);
		transform: translate3d(0, -40%, 0);
		*/
		margin-top:-40px;
	}
}

@keyframes anim-1 {
	50% {
		opacity: 0;
		/*
		-webkit-transform: translate3d(1em, 0, 0);
		transform: translate3d(1em, 0, 0);
		*/
	}
	51% {
		opacity: 0;
		/*
		-webkit-transform: translate3d(-1em, -40%, 0);
		transform: translate3d(-1em, -40%, 0);
		*/
		margin-top:-20px;
	}
	100% {
		opacity: 1;
		/*
		-webkit-transform: translate3d(0, -40%, 0);
		transform: translate3d(0, -40%, 0);
		*/
		margin-top:-40px;
	}
}

</style>