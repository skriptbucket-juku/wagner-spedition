<?php
	$success = false;
	$message = [];
	$message[] = "Vielen Dank für Ihre Kontaktanfrage";
	$allgMessage = "Es liegt eine technische Störung vor. Bitte versuchen Sie es später erneut";

	// validation expected data exists
	if(isset($_POST['name'])
		|| isset($_POST['email'])
		|| isset($_POST['message']))
	{
		$name = $_POST['name']; // required
		$email_from = $_POST['email']; // required
		$msg = $_POST['message']; // required

		$error_message = [];
		if(empty(trim($name, " ")) || strlen($name) > 50)
		{
			$error_message[] = 'Bitte geben Sie einen gültigen Vornamen ein.';
		}

		$email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
		if(empty(trim($email_from, " ")) || strlen($email_from) > 50 || !preg_match($email_exp,$email_from))
		{
			$error_message[] = 'Bitte geben Sie eine gültige E-Mail-Adresse ein.';
		}
		
		if(strlen($msg) > 1500)
		{
			$error_message[] = 'Ihre Nachricht ist zu lang. Maximal 1500 Zeichen sind erlaubt.';
		}

		if(!empty($error_message))
		{
			$message = $error_message;
			$success = false;
		}
		else
		{
			try
			{
				
				$to = "info@skriptstube.de";
				$subject = "Kontaktanfrage über Website";
				$messagesend = "Name:\n".$name;
				$messagesend .= "\n\nE-Mail:\n".$email_from;
				$messagesend .= "\n\nNachricht:\n".$msg;

				wp_mail( $to, $subject, $messagesend );
				$success = true;
			}
			catch(Exception $e)
			{
				$message[] = $allgMessage;
				$success = false;
			}

			$conn = null;
		}
	}
	else
	{
		$success = false;
		$message[] = $allgMessage;
	}

	header('Content-type: application/json');
	$arr = array ('success' => $success, 'message' => $message);
	echo json_encode($arr);
?>