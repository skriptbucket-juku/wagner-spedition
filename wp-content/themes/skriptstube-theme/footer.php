<?php
/**
 * Footer Section for our theme.
 *
 * @package skriptstube
 * @subpackage LandmarktRaeithel
 * @since skriptstube 1.0
 */
?>
</div><!-- content-div from header -->
<?php
global $post;
if (is_single($post->ID)) {
    // Closing div for microdata itemtype="http://schema.org/Blog"
    ?>
    </div>
    <?php
}
?>


<div class="rel-container contact">
    <div class="shape-contact"></div>
    <div class="contact-box">
        <p class="title">KONTAKT</p>
        <p class="info">
            Richard Wagner GmbH & Co. KG Spedition
            <br/>Heinrich-Hertz-Str. 8
            <br/>D-92224 Amberg
        </p>
        <p class="info">
            Fon: +49 9621 7720-0
            <br/>Fax: +49 9621 7720-49
            <br/>info@wagner-spedition.de
        </p>
    </div>
</div>

<footer class="footer">

    <?php
    // Übersetzungsbeispiel
    /*
    $desc_ceo = "GESCHÄFTSFÜHRER";
    if(ICL_LANGUAGE_CODE == "en")
        $desc_ceo = "MANAGING DIRECTORS";
    */

    $link_impress = "<a href=\"/impressum/\">IMPRESSUM</a>";
    if (ICL_LANGUAGE_CODE == "en")
        $link_impress = "<a href=\"/en/imprint/\">IMPRINT</a>";


    $link_dataprotection = "<a href=\"/datenschutz/\">DATENSCHUTZHINWEIS</a>";
    if (ICL_LANGUAGE_CODE == "en")
        $link_dataprotection = "<a href=\"/en/privacy-notice/\">PRIVACY NOTICE</a>";

    ?>

    <span>&copy; Wagner Spedition</span><span class="sep"> | </span><span><?=$link_impress?></span><span
            class="sep"> | </span><span><?=$link_dataprotection?></span>


</footer>
<?php wp_footer(); ?>

<script>
    //window.dataLayer = window.dataLayer || [];
    //function gtag(){dataLayer.push(arguments);}
    //gtag('js', new Date());

    //gtag('config', 'UA-');
</script>

</body>
</html>
