<?php
	/**
	 * easy debug do javascript console
	 */
	function debug_to_console($data)
	{
		if (is_array( $data ))
		{
			$output = "<script>console.log( 'Debug Objects: " . implode( ',', $data) . "' );</script>";
		}
		else
		{
			$output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";
		}
		echo $output;
	}

	/**
 	 * Enqueue scripts and styles.
 	 */
	function theme_scripts()
	{


		wp_enqueue_style( 'css_normalize', get_theme_file_uri('/assets/css/vendor/normalize.css'), array(), '4.9.0');
		wp_enqueue_style( 'css_bootstrap', get_theme_file_uri('/assets/css/vendor/bootstrap.min.css?v=2'), array(), '4.9.0');
		wp_enqueue_style( 'css_basic', get_theme_file_uri('/assets/css/custom/style.css?v=2'), array(), '4.9.0');

		// Load fontawesome
		wp_enqueue_style( 'css_fontawesome', get_theme_file_uri().'/assets/fonts/font-awesome/css/font-awesome.min.css', array(), '4.9.0' );

		// Load navigation
		wp_enqueue_script( 'css_fontawesome', get_theme_file_uri().'/assets/js/navigation.js', array(), '4.9.0' );
	}
	add_action( 'wp_enqueue_scripts', 'theme_scripts' );


	/**
	 * theme support
	 */
	function theme_prefix_setup()
	{
		add_theme_support( 'custom-logo' );
	}
	add_action( 'after_setup_theme', 'theme_prefix_setup' );

	/**
	 * registers the theme menus
	 */
	function register_theme_menus()
	{
		register_nav_menus
		(
			array
			(
				'menu_header' => __( 'Hauptmenue' ),
				'menu_footer' => __( 'Footermenue' ),
				'menu_header_top' => __( 'Topnavigation' )
			)
		);
	}
	add_action('init', 'register_theme_menus');

	// render wordpress toolbar
	function remove_admin_bar_links()
	{
		global $wp_admin_bar;

		//Remove WordPress Logo Menu Items
		$wp_admin_bar->remove_menu('wp-logo'); // Removes WP Logo and submenus completely, to remove individual items, use the below mentioned codes
		$wp_admin_bar->remove_menu('about'); // 'About WordPress'
		$wp_admin_bar->remove_menu('wporg'); // 'WordPress.org'
		$wp_admin_bar->remove_menu('documentation'); // 'Documentation'
		$wp_admin_bar->remove_menu('support-forums'); // 'Support Forums'
		$wp_admin_bar->remove_menu('feedback'); // 'Feedback'

		// Remove Comments Bubble
		$wp_admin_bar->remove_menu('comments');

		// Remove Updater
		$wp_admin_bar->remove_menu('updates');

		//Remove '+ New' Menu Items
		$wp_admin_bar->remove_menu('new-content'); // Removes '+ New' and submenus completely, to remove individual items, use the below mentioned codes
		$wp_admin_bar->remove_menu('new-post'); // 'Post' Link
		$wp_admin_bar->remove_menu('new-media'); // 'Media' Link
		$wp_admin_bar->remove_menu('new-link'); // 'Link' Link
		$wp_admin_bar->remove_menu('new-page'); // 'Page' Link
		$wp_admin_bar->remove_menu('new-user'); // 'User' Link

		// remove contanct bank plugin integration
		$wp_admin_bar->remove_menu('contact_bank_links');
	}
	add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );

	// renders admin menu for user-roles
	function render_admin_menu_pages()
	{
		global $user_ID;

		if (!current_user_can('administrator'))
		{
			remove_menu_page( 'admin-bar-dashboard-control');
			remove_menu_page( 'contact_dashboard');
		}
	}
	add_action( 'admin_init', 'render_admin_menu_pages' );

	// add custom color to editor
	function my_mce4_options($init)
	{
		$default_colours = '"000000", "Black",
						  "993300", "Burnt orange",
						  "333300", "Dark olive",
						  "003300", "Dark green",
						  "003366", "Dark azure",
						  "000080", "Navy Blue",
						  "333399", "Indigo",
						  "333333", "Very dark gray",
						  "800000", "Maroon",
						  "FF6600", "Orange",
						  "808000", "Olive",
						  "008000", "Green",
						  "008080", "Teal",
						  "0000FF", "Blue",
						  "666699", "Grayish blue",
						  "808080", "Gray",
						  "FF0000", "Red",
						  "FF9900", "Amber",
						  "99CC00", "Yellow green",
						  "339966", "Sea green",
						  "33CCCC", "Turquoise",
						  "3366FF", "Royal blue",
						  "800080", "Purple",
						  "999999", "Medium gray",
						  "FF00FF", "Magenta",
						  "FFCC00", "Gold",
						  "FFFF00", "Yellow",
						  "00FF00", "Lime",
						  "00FFFF", "Aqua",
						  "00CCFF", "Sky blue",
						  "993366", "Red violet",
						  "FFFFFF", "White",
						  "FF99CC", "Pink",
						  "FFCC99", "Peach",
						  "FFFF99", "Light yellow",
						  "CCFFCC", "Pale green",
						  "CCFFFF", "Pale cyan",
						  "99CCFF", "Light sky blue",
						  "CC99FF", "Plum"';

		$custom_colours =  '"009a55", "Landmarkt Gr�n",
							"f8c526", "Landmarkt Gelb"';

		// build colour grid default+custom colors
		$init['textcolor_map'] = '['.$custom_colours.','.$default_colours.']';

		// enable 6th row for custom colours in grid
		$init['textcolor_rows'] = 6;

		return $init;
	}
	add_filter('tiny_mce_before_init', 'my_mce4_options');

// Relative URLs
$filters = array(
	'post_link',       // Normal post link
	'post_type_link',  // Custom post type link
	'page_link',       // Page link
	'attachment_link', // Attachment link
	'get_shortlink',   // Shortlink

	'post_type_archive_link',    // Post type archive link
	'get_pagenum_link',          // Paginated link
	'get_comments_pagenum_link', // Paginated comment link

	'term_link',   // Term link, including category, tag
	'search_link', // Search link

	'day_link',   // Date archive link
	'month_link',
	'year_link',
);

foreach ( $filters as $filter ) {
	add_filter( $filter, 'wp_make_link_relative' );
}

// Relative URLs for images
function switch_to_relative_url($html, $id, $caption, $title, $align, $url, $size, $alt)
{
	$imageurl = wp_get_attachment_image_src($id, $size);
	$relativeurl = wp_make_link_relative($imageurl[0]);
	$html = str_replace($imageurl[0],$relativeurl,$html);

	return $html;
}
add_filter('image_send_to_editor','switch_to_relative_url',10,8);

function add_my_script() {
	echo "<script type='text/javascript' src='/wp-content/plugins/bb-plugin/js/jquery.js'></script>";
}
add_action( 'wp_head', 'add_my_script', 1 );

add_theme_support( 'post-thumbnails' );

//add pagination-function
if (!function_exists('post_pagination'))
{
	function post_pagination()
	{
		global $wp_query;
		$pager = 999999999; // need an unlikely integer

		echo paginate_links( array(
			'base' => "%_%",
			'format'             => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $wp_query->max_num_pages
		));
	}
}

// Standard Size Thumbnail
if(false === get_option("thumbnail_crop")) {
add_option("thumbnail_crop", "1"); }
else {
update_option("thumbnail_crop", "1");
}

// Medium Size Thumbnail
if(false === get_option("medium_crop")) {
add_option("medium_crop", "1"); }
else {
update_option("medium_crop", "1");
}

// Large Size Thumbnail
if(false === get_option("large_crop")) {
add_option("large_crop", "1"); }
else {
update_option("large_crop", "1");
}



// include files via shortcode
// Example: [include filepath='/deine-datei.php']
function include_file($atts) {
	extract(shortcode_atts(array('filepath' => 'NULL'), $atts));
	if ($filepath!='NULL' && file_exists(TEMPLATEPATH.$filepath)){
	ob_start();
	include(TEMPLATEPATH.$filepath);
	$content = ob_get_clean();
	return $content;
	}
}

add_shortcode('include', 'include_file');



// breadcrumb
function nav_breadcrumb() {

 $delimiter = '&raquo;';
 $home = 'Startseite'; 
 $before = '<span class="current-page">'; 
 $after = '</span>'; 
 
 if ( !is_home() && !is_front_page() || is_paged() ) {

 echo '<div class="breadcrumb">Sie sind hier: ';
 
 global $post;
 $homeLink = get_bloginfo('url');
 echo '<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
 
 if ( is_category()) {
 global $wp_query;
 $cat_obj = $wp_query->get_queried_object();
 $thisCat = $cat_obj->term_id;
 $thisCat = get_category($thisCat);
 $parentCat = get_category($thisCat->parent);
 if ($thisCat->parent != 0) echo("111".get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
 echo $before . single_cat_title('', false) . $after;
 
 } elseif ( is_day() ) {
 echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
 echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
 echo $before . get_the_time('d') . $after;
 
 } elseif ( is_month() ) {
 echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
 echo $before . get_the_time('F') . $after;
 
 } elseif ( is_year() ) {
 echo $before . get_the_time('Y') . $after;
 
 } elseif ( is_single() && !is_attachment() ) {
 if ( get_post_type() != 'post' ) {
 $post_type = get_post_type_object(get_post_type());
 $slug = $post_type->rewrite;
 echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . ' ';
 echo $before . get_the_title() . $after;
 } else {
 $cat = get_the_category(); $cat = $cat[0];
 $linkmodify = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
 $linkmodify = str_replace("/category/blog/", "/blog/", $linkmodify);
 echo $linkmodify;
 echo $before . get_the_title() . $after;
 }
 
 } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
 $post_type = get_post_type_object(get_post_type());
 echo $before . $post_type->labels->singular_name . $after;
 

 } elseif ( is_attachment() ) {
 $parent = get_post($post->post_parent);
 $cat = get_the_category($parent->ID); $cat = $cat[0];
 echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
 echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';
 echo $before . get_the_title() . $after;
 
 } elseif ( is_page() && !$post->post_parent ) {
 echo $before . get_the_title() . $after;
 
 } elseif ( is_page() && $post->post_parent ) {
 $parent_id = $post->post_parent;
 $breadcrumbs = array();
 while ($parent_id) {
 $page = get_page($parent_id);
 $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
 $parent_id = $page->post_parent;
 }
 $breadcrumbs = array_reverse($breadcrumbs);
 foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
 echo $before . get_the_title() . $after;
 
 } elseif ( is_search() ) {
 echo $before . 'Ergebnisse für Ihre Suche nach "' . get_search_query() . '"' . $after;
 
 } elseif ( is_tag() ) {
 echo $before . 'Beiträge mit dem Schlagwort "' . single_tag_title('', false) . '"' . $after;

 } elseif ( is_404() ) {
 echo $before . 'Fehler 404' . $after;
 }
 
 if ( get_query_var('paged') ) {
 if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
 echo ': ' . __('Seite') . ' ' . get_query_var('paged');
 if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
 }
 
 echo '</div>';
 
 } 
} 


//add_image_size( 'custom400x400', 400, 400, array( 'left', 'top' ) ); // Hard crop left top
add_image_size( 'custom400x400', 400, 400, true );
add_image_size( 'custom750x750', 750, 750 );
//add_filter( 'image_size_names_choose', 'custom400x400' );


/*
* Resize images dynamically using wp built in functions
* Victor Teixeira
*
* php 5.2+
*
* Exemplo de uso:
*
* <?php
* $thumb = get_post_thumbnail_id();
* $image = vt_resize($thumb, '', 140, 110, true);
* ?>
* <img src="<?php echo $image[url]; ?>" width="<?php echo $image[width]; ?>" height="<?php echo $image[height]; ?>" />
*
* @param int $attach_id
* @param string $img_url
* @param int $width
* @param int $height
* @param bool $crop
* @return array
*/
if(!function_exists('vt_resize')){
    function vt_resize($attach_id = null, $img_url = null, $width, $height, $crop = false){
    if($attach_id){
        // this is an attachment, so we have the ID
        $image_src = wp_get_attachment_image_src($attach_id, 'full');
        $file_path = get_attached_file($attach_id);
    } elseif($img_url){
        // this is not an attachment, let's use the image url
        $file_path = parse_url($img_url);
        $file_path = $_SERVER['DOCUMENT_ROOT'].$file_path['path'];
        // Look for Multisite Path
        if(file_exists($file_path) === false){
            global $blog_id;
            $file_path = parse_url($img_url);
            if(preg_match('/files/', $file_path['path'])){
                $path = explode('/', $file_path['path']);
                foreach($path as $k => $v){
                    if($v == 'files'){
                        $path[$k-1] = 'wp-content/blogs.dir/'.$blog_id;
                    }
                }
                $path = implode('/', $path);
            }
            $file_path = $_SERVER['DOCUMENT_ROOT'].$path;
        }
        //$file_path = ltrim( $file_path['path'], '/' );
        //$file_path = rtrim( ABSPATH, '/' ).$file_path['path'];
        $orig_size = getimagesize($file_path);
        $image_src[0] = $img_url;
        $image_src[1] = $orig_size[0];
        $image_src[2] = $orig_size[1];
    }
    $file_info = pathinfo($file_path);
    // check if file exists
    $base_file = $file_info['dirname'].'/'.$file_info['filename'].'.'.$file_info['extension'];
    if(!file_exists($base_file))
    return;
    $extension = '.'. $file_info['extension'];
    // the image path without the extension
    $no_ext_path = $file_info['dirname'].'/'.$file_info['filename'];
    $cropped_img_path = $no_ext_path.'-'.$width.'x'.$height.$extension;
    // checking if the file size is larger than the target size
    // if it is smaller or the same size, stop right here and return
    if($image_src[1] > $width){
        // the file is larger, check if the resized version already exists (for $crop = true but will also work for $crop = false if the sizes match)
        if(file_exists($cropped_img_path)){
            $cropped_img_url = str_replace(basename($image_src[0]), basename($cropped_img_path), $image_src[0]);
            $vt_image = array(
                'url'   => $cropped_img_url,
                'width' => $width,
                'height'    => $height
            );
            return $vt_image;
        }
        // $crop = false or no height set
        if($crop == false OR !$height){
            // calculate the size proportionaly
            $proportional_size = wp_constrain_dimensions($image_src[1], $image_src[2], $width, $height);
            $resized_img_path = $no_ext_path.'-'.$proportional_size[0].'x'.$proportional_size[1].$extension;
            // checking if the file already exists
            if(file_exists($resized_img_path)){
                $resized_img_url = str_replace(basename($image_src[0]), basename($resized_img_path), $image_src[0]);
                $vt_image = array(
                    'url'   => $resized_img_url,
                    'width' => $proportional_size[0],
                    'height'    => $proportional_size[1]
                );
                return $vt_image;
            }
        }
        // check if image width is smaller than set width
        $img_size = getimagesize($file_path);
        if($img_size[0] <= $width) $width = $img_size[0];
            // Check if GD Library installed
            if(!function_exists('imagecreatetruecolor')){
                echo 'GD Library Error: imagecreatetruecolor does not exist - please contact your webhost and ask them to install the GD library';
                return;
            }
            // no cache files - let's finally resize it
            $new_img_path = image_resize($file_path, $width, $height, $crop);
            $new_img_size = getimagesize($new_img_path);
            $new_img = str_replace(basename($image_src[0]), basename($new_img_path), $image_src[0]);
            // resized output
            $vt_image = array(
                'url'   => $new_img,
                'width' => $new_img_size[0],
                'height'    => $new_img_size[1]
            );
            return $vt_image;
        }
        // default output - without resizing
        $vt_image = array(
            'url'   => $image_src[0],
            'width' => $width,
            'height'    => $height
        );
        return $vt_image;
    }
}

//Google recaptcha in different language

function language_recaptcha_scripts()
{
	if(null !== ICL_LANGUAGE_CODE && ICL_LANGUAGE_CODE == "en")
	{
		wp_deregister_script( 'google-recaptcha' );

		$url = 'https://www.google.com/recaptcha/api.js';
		$url = add_query_arg( array(
			'onload' => 'recaptchaCallback',
			'render' => 'explicit',
			'hl' => 'en'), $url ); // es is the language code for Spanish language

		wp_register_script( 'google-recaptcha', $url, array(), '2.0', true );
	}
	else if(null !== ICL_LANGUAGE_CODE && ICL_LANGUAGE_CODE == "zh")
	{
		wp_deregister_script( 'google-recaptcha' );

		$url = 'https://www.google.com/recaptcha/api.js';
		$url = add_query_arg( array(
			'onload' => 'recaptchaCallback',
			'render' => 'explicit',
			'hl' => 'zh'), $url ); // es is the language code for Spanish language

		wp_register_script( 'google-recaptcha', $url, array(), '2.0', true );
	}
}

add_action( 'wpcf7_enqueue_scripts', 'language_recaptcha_scripts', 11 );

?>
