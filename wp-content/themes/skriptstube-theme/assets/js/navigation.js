/**
 * navigation.js
 *
 * Handles toggling the navigation menu for small screens.
 */
( function() {
} )();
jQuery(document).ready(function() {

	// Language Selection
	if(jQuery("nav ul.menu").length > 0)
	{
		
		// Aktuelle Sprache
		$langCurrent = jQuery("html").attr("lang").substr(0,2);
		$langDescription = $langCurrent;
		/*		
		$langDescription = "Deutsch";
		if($langCurrent == "zh")
		{
			$langDescription = "中文";
		}
		else if($langCurrent == "en")
		{
			$langDescription = "English";
		}
		*/
		
		// Grundmenü erweitern
		$langSelector = "<li class='menu-item menu-item-type-post_type menu-item-object-page iLangSelectionCurrentAdd'><span id='iNavSelection'><i class='fa fa-globe'></i> "+$langDescription+" <i class='fa fa-angle-down'></i></span></li>";
		jQuery("nav ul.menu").append($langSelector);
		
		
		// Weitere Sprachen
		$langOthers = [];
		if(jQuery("#lang_switch a").length > 0)
		{
			jQuery.each(jQuery("#lang_switch a"), function(index, value)
			{
				$langOthers.push([jQuery(value).text(), jQuery(value).attr("href")]);
			});
		}

		
		// Generiere Submenü
		$subMenu = "<div class='iLangSelectionSubmenu'>";
		//$subMenu += "<p><a>"+$langCurrent+"</a></p>";
		for(i=1; i<= $langOthers.length; i++)
		{
			//$langDescription = $langOthers[i-1][0];
			
			$langDescription = "DE - Deutsch";

			if($langOthers.length>0)
			{
				if($langOthers[i-1][0] == "zh")
				{
					$langDescription = "ZH - 中文";
				}
				else if($langOthers[i-1][0] == "en")
				{
					$langDescription = "EN - English";
				}
			}

			
			$subMenu += "<p data-open-href='"+$langOthers[i-1][1]+"'><a href='"+$langOthers[i-1][1]+"'>"+$langDescription+"</a></p>";
		}
		$subMenu += "</div>";
		
		jQuery(".iLangSelectionCurrentAdd").append($subMenu);
		
		//console.log($subMenu);
		//console.log($langCurrent);
		//console.log($langOthers);
		
		// Klickfunktion auf Länderauswahl
		jQuery(".iLangSelectionCurrentAdd").click(function(){
			if(jQuery(this).hasClass("hover"))
			{
				jQuery(this).removeClass("hover");
			}
			else{
				jQuery(this).addClass("hover");
			}
			//jQuery(this).toggleClass("hover");
		});
		
	}

    $( "#goup" ).click(function() {
    	$('html, body').animate({
        	scrollTop: 0
        	}, ($(document).scrollTop())/4, "linear");
    });
	$(window).scroll(function(e) {
		if(document.body.scrollTop > 180)
		{
			$("#goup").addClass("sticky");
		}
		else{
			$("#goup").removeClass("sticky");
		}
	});
	
	
	$( ".scrollme, .maskbg h2" ).click(function() {
    	$('html, body').animate({
        	scrollTop: ($(window).width() * 0.80)
			//($(window).height() - $(window).width() * 0.20)
        	}, 800, "linear");
    });
	

	function sticky()
    {
		var element_to_stick = $('nav, body');

		if($(window).scrollTop() > 32 || $("body").scrollTop() > 32)
		{
            element_to_stick.addClass('sticky');
        } else {
            element_to_stick.removeClass('sticky');
        }
    }
	$(window).scroll(sticky);
    sticky();
	
	$("#mobile-burger").click(function(){
		
		if(!$("nav").hasClass("show"))
		{
			$("nav").addClass("show");
		}
		else
		{
			$("nav").removeClass("show");
			$("nav").removeClass("newscrollposition");
		}
				
		$( "nav ul" ).slideToggle( "slow", function() {
			// Animation complete.
		});

	});
	
	$(window).resize(function(){
		
		var wWidth = $(window).width();
		var wHeight = $(window).height();
		var v80Proz = Math.floor($(window).height() * 0.8);
		var v16to9 = Math.floor($(window).width() / 16 * 9);
		//console.log("breite:" + wWidth + ", höhe:" + wHeight + ", max(80):" + v80Proz+ ", 16:9:" + v16to9);
		var diff = 260 - v16to9;
	
		if($(window).width() >= 992)
		{
			//$("nav ul").css("display", "block");
			$("nav ul").removeAttr("style");
			$("nav").removeClass("show");
		}
	});
	
	$("ul#menu-hauptmenue > li.menu-item-has-children").each(function( index ) {
		$(this).prepend("<i class=\"fa fa-caret-down submenuopen\" aria-hidden=\"true\"></i>");
	});
	
	$(".submenuopen").click(function(){

		$("nav").addClass("newscrollposition");
		
		if($(this).parent("li").hasClass("showm")){
			$("#menu-hauptmenue .showm").removeClass("showm");			
		}
		else{
			$("#menu-hauptmenue .showm").removeClass("showm");
			$(this).parent("li").addClass("showm");				
		}	
		
		$('html,body').animate({ scrollTop: 0 }, 'slow');
        return false; 
		
	});
		
});


	// Open URLs from other Elements
	jQuery(document)
		.on("click touchstart", "[data-open-href]", function() {
			$url = jQuery(this)
				.data("open-href");
			window.location = $url;
		});