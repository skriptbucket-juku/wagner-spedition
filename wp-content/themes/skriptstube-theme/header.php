<?php
/**
 * The header for our theme.
 *
 * @package skriptstube
 * @subpackage LandmarktRaeithel
 * @since skriptstube 1.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
		<link rel="icon" href="<?php echo(get_template_directory_uri());?>/assets/img/fav_icon.png" />
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo(get_template_directory_uri());?>/assets/img/fav_icon.png">
		<title><?php echo(wp_title(""));?></title>
		<meta name="google-site-verification" content="kAyGcEgQ0xb8a5saYfqfcIzdnqkhK_HvKquapNeLkuk" />
		<meta name="theme-color" content="#4f9fa6" />
		<meta name="msapplication-navbutton-color" content="#4f9fa6" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
		<meta name="format-detection" content="telephone=no">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110297723-1"></script>
					
	<?php
		// wordpress includes
		wp_head();
	?>
	<style type="text/css">
		html {margin-top: 0px !important;}
        li.iLangSelectionCurrentAdd{display:none!important;}
        #content{overflow:hidden;}
		<?php
			if(ICL_LANGUAGE_CODE == "zh")
			{
				?>		
				.article_container article p, .fl-rich-text p, #content .fl-rich-text ul li, #content .fl-accordion ul li{
					font-size:110%!important;
				}
				<?php
			}
		?>
	</style>
	</head>
						
	<body <?php body_class(); ?>>
		<header>


			<nav>
				<?php
					$homelink="/";
					if(ICL_LANGUAGE_CODE == "en")
						$homelink="/en/";
					else if(ICL_LANGUAGE_CODE == "zh")
						$homelink="/zh/";
				?>
				<a id="logo" href="<?php echo($homelink);?>"><div></div></a>
				<a id="mobile-burger">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<?php
					$menu = 'menu_header';
					if (has_nav_menu($menu))
					{
						wp_nav_menu(
							array(
								'theme_location' => $menu
							)
						);
					}
					else
					{
						wp_page_menu();
					}
				?>
			</nav>

			<?php
			function custom_lang_switch_all_lang(){
				$languages = apply_filters( 'wpml_active_languages', NULL, 'skip_missing=0&orderby=code' );
				if( !empty( $languages ) ){
					echo '<ul id="menu-menu-principal" class="nav navbar-nav navbar-right">';
					foreach( $languages as $l ){
						$url = $l['url'];
						$active = "";
						$onclick = "";
						if( $l['active'] ) {
							$url = "#";
							$active = ' menu-item-language-current';
							$onclick = ' onclick="return false"';
						}
			 
						echo '<li class="menu-item menu-item-language menu-item-has-children'.$active.'">';
						echo '<a href="'.$url.'" ' .$onclick. '>' . $l['translated_name'] . '</a>';
						echo '</li>';
					}
					echo '</ul></div>';
				}
			}
			function custom_lang_switch(){
				$languages = apply_filters( 'wpml_active_languages', NULL, 'skip_missing=0&orderby=code' );
				if( !empty( $languages ) ){
					$result="";
					foreach( $languages as $l ){
						$url = $l['url'];
						$active = "";
						$onclick = "";
						if( !$l['active'] ) {
							//$result.="<a id=\"lang_switch\" href=\"".$url."\" ".$onclick.">".$l['translated_name']."</a>";
							if($result != "")
								$result.= " | ";
							$result.="<a href=\"".$url."\" ".$onclick.">".$l['language_code']."</a>";
							
						}
					}
					if($result != "")
					{
						$result = "<div id=\"lang_switch\">".$result."</div>";
					}
					
					
					echo($result);
				}
			}
			custom_lang_switch();
			?>			
		</header>
		<?php
			global $post;
			if(is_single($post->ID))
			{
				?>
					<div itemid="<?php get_site_url();?>" itemscope itemtype="http://schema.org/Blog">
				<?php
			}

			/*
			if (1==1)//is_front_page() == false)
			{
				$thumb = get_post_thumbnail_id();
				$image = vt_resize($thumb, '', 1920, 1000, true);
				//$post_title = html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8');
				$post_title = html_entity_decode(get_post_meta($post->ID, 'Banner - Titel (H1)', true), ENT_COMPAT, 'UTF-8');
				$post_subtitle = html_entity_decode(get_post_meta($post->ID, 'Banner - Subtitel', true), ENT_COMPAT, 'UTF-8');
				?>
				<div class="maskbg">
					<h1<?php if(is_single($post->ID)){echo(' itemprop="headline"');}?>><?php echo ($post_title); ?></h1>
					<h2><?php echo ($post_subtitle); ?></h2>
				</div>
				<div class="feature_image" <?php if (empty(get_the_post_thumbnail_url()) == false) { ?>style="background-image:url(<?php echo htmlspecialchars($image[url]) ?>)" <?php } ?>>
					<div class="overlay">
						<div class="content">
							<?php
								// Original position for page title
							?>
						</div>
					</div>
				</div>
				<?php
			}
			*/
		?>
		<div id="content">
		<?php 
			
			//if (function_exists('nav_breadcrumb')) nav_breadcrumb();
		?>
