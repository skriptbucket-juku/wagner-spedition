<?php
	get_header();
?>

<?php
$img1 = "/wp-content/uploads/img1.jpg";
$img2 = "/wp-content/uploads/img2.jpg";
?>
<style>

.outer-header {
	margin-bottom: 200px;
	height: 400px;
	position:relative;
}

.inner-header {
	max-width: 1000px;
	margin: 0px auto;
	height: 100%;
	position: relative;
}

/* ------------------------------------------------- */
/* Linke Bildbox */
.shape-a3-img{
	height: 400px;
	width:100%;
	position:absolute;

	background-image:url(<?=$img1?>);	
	background-size:cover;
	background-position:center;
}

/* Abdunklung */
.shape-a3-img:after{
	content:"";
	position:absolute;
	top:0px;
	width:100%;
	height:400px;
	background-color:rgba(0,0,0,.6);
}

/* ------------------------------------------------- */
/* Rechte Bildbox */
.shape-c3{
	height: 540px;
	width: calc((100% - 1000px) / 2 + 460px);
	position:absolute;
	right:0px;
	top: 0px;
	
	background-color:#ffffff;
	-webkit-clip-path: polygon(440px 0%, 100% 0%, 100% 100%, 0% 100%);
}

.shape-c3-img{
	height: 100%;
	width: 100%;
	position:absolute;
	right:0px;
	top: 0px;
		
	background-image:url(<?=$img2?>);	
	background-size:cover;
	background-position:center;
}

.shape-c3-img,
.shape-c3-img:after{
	-webkit-clip-path: polygon(
	calc(440px + 20px) 0%, 
	100% 0%, 
	100% 100%, 
	calc(0% + 20px) 100%);
}

/* Rote Abdunklung */
.shape-c3-img:after{
	content:"";
	position:absolute;
	top:0px;
	width:100%;
	height:100%;
	background-color:rgba(255,55,0,.6);
}





/* ------------------------------------------------- */
/* Title und Beschriftungen */
.imgtitle3,
.maintitle3{
	line-height: 1;
}
	

.imgtitle3{
	position: absolute;
    bottom: -100px;
    font-size: 40px;
    text-transform: uppercase;
    color: #ffffff;
    z-index: 20;
    width: 30%;
    right: -2%;
	font-weight:bold;
}

.maintitle3{
	position: absolute;
    bottom: -220px;
    font-size: 40px;
    text-transform: uppercase;
    color: #000000;
    z-index: 20;
    width: 33%;
    left: 0%;
    border-top: 5px solid #000000;
    font-weight: bold;
    text-align: left;
}







/* ------------------------------------------------- */
/* Responsive Darstellung */
@media screen and (max-width:1000px)
{
	.shape-a3-img{
		display:none;
	}
	
	.shape-c3,
	.shape-c3-img,
	.shape-c3-img:after
	{
		-webkit-clip-path: polygon(0% 0%, 100% 0%, 100% 100%, 0% 100%);
	}
	
	.outer-header,
	.shape-c3,
	.shape-c3-img,
	.shape-c3:after{
		height:200px;
		width:100%;
	}
	
	.imgtitle3{
		font-size:30px;
		width:100%;
		bottom:20px;
		left:0px;
		text-align:center;
		padding:20px;
		box-sizing:border-box;
	}

	.maintitle3{
		left:0%;
		padding:20px;
		width:80%;
	}
}

.blogpostcontent{
	
	max-width: 1000px;
	margin: 0px auto;
	margin-top:100px;
	position: relative;
	padding:20px;
}

.otherblogs2 {
    display: block;
    margin: 0px auto;
    padding: 20px 20px;
    max-width: 1000px;
    font-size: 12px;
    position: relative;
    overflow: hidden;
}

.next:hover a,
.prev:hover a{
	color:#d32e16;
}

</style>

<div class="outer-header">

	<div class="shape-a3-img">
	</div>

	<div class="inner-header">
		<div class="imgtitle3">
			Aktuelles rund um die Spedition
		</div>

		<div class="maintitle3">
			<?php
			the_title();
			?>
		</div>
	</div>
	
	<div class="shape-c3">
		<div class="shape-c3-img">
		</div>
	</div>
</div>

<?php	
	echo('<div itemprop="description">');
	while (have_posts()) : the_post();
				
		?>

		<div class="blogpostcontent">
		<br/>
		<?php echo(get_the_date( 'd.m.Y' ));?>
		<br/><br/>
		<?php
			$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
			if($thumbnail[0] != "")
			{
				// Bild ausgeben
				echo('<img style="max-width:100%;" src="'.$thumbnail[0].'"/><br/><br/>');
			}
			
		
			the_content();
		?>
		</div>		
		
		<?php
	endwhile;
	echo('</div>');	
	
	// Author details
	//$display_name = get_the_author_meta( 'display_name', $post->post_author );
	//$user_description = get_the_author_meta( 'user_description', $post->post_author );
	//$user_avatar = get_avatar( get_the_author_meta('user_email') , 300 );

	?>
	<time itemprop="datePublished" content="<?php echo(get_the_date( 'Y-m-d\TG:i:s' ));?>"></time>
	<div itemprop="inLanguage" content="de-DE"></div>
	
	
	<div class="otherblogs2">
		<h3>Interesse an weiteren Artikeln?</h3>
		<div class="next">
			<?php next_post_link("Nächster Artikel: %link"); ?>
		</div>
		<div class="prev">
			<?php previous_post_link("Vorheriger Artikel: %link"); ?>
		</div>
		<div class="prev">
			<a href="/aktuelles">Zurück zu Aktuelles</a>
		</div>
	</div>
	
	
	<?php
	get_footer();
?>