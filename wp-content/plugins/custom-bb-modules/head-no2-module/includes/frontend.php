<?php


$fullsize_path_img1 = basename(get_attached_file($settings->photo));
$fullsize_path_img2 = basename(get_attached_file($settings->photo2));


$img1 = "/wp-content/uploads/" . $fullsize_path_img1;
$img2 = "/wp-content/uploads/" . $fullsize_path_img2;

//echo("titel:".$settings->title);
//echo("<br/>titel:".$fullsize_path_img1);
//echo("<br/>titel:".$fullsize_path_img2);
//echo("<br/>titel:".$fullsize_path_img3);
//<img src="/wp-content/uploads/<?=$fullsize_path_img1?_>" />

?>

<style>


    .outer-header {
        margin-bottom: 200px;
    }

    .outer-header,
    .shape-a,
    .shape-c2 {
        height: 400px;

    }

    .inner-header {
        max-width: 1000px;
        margin: 0px auto;
        height: 100%;
        position: relative;
    }


.shape-b2,
.shape-b2:after,
.shape-b2-img {
	width:100%;
	height:540px;
	transition: all 300ms;
}

.shape-b2{
	width:calc((100vw - 1000px) / 2 + 1000px);
}


.shape-b2{
	-webkit-clip-path: polygon(0% 0%, 100% 0%, calc(100% - 460px) 100%, 0% 100%);
}

.shape-b2:after,
.shape-b2-img{
	-webkit-clip-path: polygon(
		calc(0% + 0px) 0%,
		calc(100% - 20px) 0%,
		calc(100% - 460px - 20px) 100%,
		calc(0% + 0px) 100%
	);
}

/* Weißer Ranb */
.shape-b2 {
	margin:0px auto;
	background-color:#ffffff;
	
	position:absolute;
	top:0px;
	
	z-index:10;
}

/* Einfärbung Bild */
.shape-b2:after{
	content:"";

	position:absolute;
	top:0px;
	
	background-color:rgba(0,0,0,.6);
}

.shape-c2:after{
	content:"";

	position:absolute;
	top:0px;
	height:100%;
	width:100%;
	
	background-color:rgba(255,55,0,.5);
}

/* Hintergrundbild */
.shape-b2-img {	
	background-image:url(<?=$img1?>);	
	background-size:cover;
}

.shape-a2,
.shape-c2{
	width:50%;
	
	position:absolute;
	top:0px;
		
	background-position:center;
	background-size:cover;
}

.shape-c2{
	right:0px;
	background-image:url(<?=$img2?>);
	
}
.imgtitle,
.maintitle{
	line-height: 1;
}
	

.imgtitle{
	position: absolute;
    bottom: -100px;
    font-size: 40px;
    text-transform: uppercase;
    color: #ffffff;
    z-index: 20;
    width: 30%;
    left: 20%;
	font-weight:bold;
}

.maintitle{
	position: absolute;
    bottom: -200px;
    font-size: 40px;
    text-transform: uppercase;
    color: #000000;
    z-index: 20;
    width: 32%;
    left: 63%;
	border-top:5px solid #000000;
	font-weight:bold;
	text-align:right;
}


@media screen and (max-width:1000px)
{
	.shape-a2, .shape-c2{
		display:none;
	}
	
	.shape-b2,
	.shape-b2-img,
	.shape-b2:after
	{
		-webkit-clip-path: polygon(0% 0%, 100% 0%, 100% 100%, 0% 100%);
		height:200px;
		background-position:center;
	}
	
	.outer-header,
	.shape-b2,
	.shape-b2-img,
	.shape-b2:after{
		height:200px;
		width:100%;
	}
	
	.imgtitle{
		font-size:30px;
		width:100%;
		bottom:20px;
		left:0px;
		text-align:center;
		padding:20px;
		box-sizing:border-box;
	}

	.maintitle{
		left:0%;
		padding:20px;
	}

	.content{
		margin-top:180px;
	}

}

.shape-b2-img{
	animation-name: iliketomoveit;
	animation-duration: 15s;
	animation-iteration-count: infinite;
	
	background-size:120%;
}

.maintitle p {
	margin-bottom: 0px;
    line-height: 110%;
}

</style>




<div class="outer-header">

		<div class="shape-b2">
			<div class="shape-b2-img">
			</div>
		</div>

	<div class="inner-header">
		<div class="imgtitle">
			<?= $settings->title ?>
		</div>
		
		
		<div class="maintitle">
			<?= $settings->subtitle ?>
		</div>
		
	</div>
	
	<div class="shape-c2">
	</div>
</div>
