<?php
$title = $settings->title;
?>
<style>
	.blog .img{
		width:100%;
		height:300px;
		background-size:cover;
		background-size:100%;
		background-position:center;
		transition-duration:.4s;
	}

	.blog .title{
		border-top:2px solid #333333;
		display:inline-block;
		margin:20px 0px 10px 0px;
		padding-right:20px;
		text-transform:uppercase;
		font-weight:bold;
		font-size:120%;
		transition-duration:.4s;
	}

	.blog .desc{
		text-align:justify;
	}
	
	.blog a:hover{
		text-decoration:none;
		color:#333333;
	}
	
	
	.blog a:hover .title{
		color:#d32e16;
		border-top:2px solid #d32e16;
	}
	.blog a:hover .img{
		background-size:110%;
	}
	
</style>

<?php
// Get the query data.
$query  = FLBuilderLoop::query( $settings );
// Render the posts.
if ( $query->have_posts() )
{


	// Gesamtüberschrift
	if($title != "")
	{
		echo("<h2>".$title."</h2>");
	}


	echo('<div class="content my-0">');//#1
	$validEntry = 0;

	foreach ($query->posts as $post)
	{
		
		if($post->post_status != "publish")
			continue;
		
		
		$validEntry++;

		if($validEntry%2 == 1)
		{
			echo('<div class="row mx-0 blog">');//#2
		}
		
		
		//ID
		$id = $post->ID;
		
		// Überschrift
		$title = $post->post_title;
		// Beschreibungstext
		$text = $post->post_content;
		// Bildpfad
		$bild = get_the_post_thumbnail_url($id);
		
		$link = "/aktuelles/".$post->post_name;
		
		
		?>
		<div class="col-md-6 my-5">
			<a href="<?=$link?>">
				<div class="img" style="background-image:url(<?=$bild?>);"></div>
				<div class="title"><?=$title?></div>
				<div class="desc">
					<?=$text?>
				</div>
			</a>
		</div>
		<?php
		
		
		if($validEntry%2 == 0)
		{
			echo('</div>');//#2
		}
	}
	echo('</div>');//#1
}
?>