/*
---------------------------------------------
Elements at 100VH
---------------------------------------------
*/

.fl-node-<?php echo $id; ?> .imageBoxWidget.full_height{
	/*full height minus height of header and footer*/
	min-height: calc(100vh - 150px - 46px);
}

body.logged-in .fl-node-<?php echo $id; ?> .imageBoxWidget.full_height{
	/*Dammit WP-Admin Bar*/
	min-height: calc(100vh - 150px - 46px - 32px);
}

@media screen and (max-width: 782px) {
	body.logged-in .fl-node-<?php echo $id; ?> .imageBoxWidget.full_height{
		/*Dammit WP-Admin Bar*/
		min-height: calc(100vh - 150px - 46px - 46px);
	}
}

html.fl-builder-edit .fl-node-<?php echo $id; ?> .imageBoxWidget.full_height{
	/*Fix the PageBuilder Bar*/
	min-height: calc(100vh - 150px - 46px - 43px);
}

/*
---------------------------------------------
Widget-Styling
---------------------------------------------
*/

<?php
	$bilddatei = esc_url($settings->photo_src);
	$size = getimagesize( $bilddatei);
	$cssAddition = "background-size: ".$size[0]."px ".$size[1]."px; min-height: ".$size[1]."px;";
?>

.fl-node-<?php echo $id; ?> .imageBoxWidget{
  width:100%;
  background-repeat: no-repeat;
  background-size: <?php echo $cssAddition; ?>;
  background-position: center bottom;

  min-height: 400px;
  margin-bottom:0px;
  
  position: relative;
}

.fl-node-<?php echo $id; ?> .imageBoxWidget .imageBoxWidgetText
{
	display: none;
	position: absolute;
	top: <?php echo $settings->from_top; ?>%;
	right: <?php echo $settings->from_right; ?>%;
}

.fl-node-<?php echo $id; ?> .imageBoxWidget .imageBoxWidgetText:first-child
{
	display: block;
}