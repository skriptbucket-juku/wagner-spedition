<?php

/**
 * @class BlogModule
 */
class BlogModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('BlogModule', 'fl-builder'),
			'description'   	=> __('Darstellung der Blogartikel', 'fl-builder'),
			'category'      	=> __('Wagner-Module', 'fl-builder'),
			'partial_refresh'	=> true
		));

		$this->add_css('jquery-bxslider');
		$this->add_js('jquery-bxslider');
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('BlogModule', array(
	'general'       => array(
		'title'         => __('General', 'fl-builder'),
		'sections'      => array(
			'general'       => array(
				'title'         => '',
				'fields'        => array(

					'title'          => array(
						'type'          => 'editor',
						'label'         => 'Blog-Titel',
						'rows'          => 2,
						'wpautop'		=> false,
						'preview'         => array(
							'type'             => 'text',
							'selector'         => '.fl-rich-text',
						),
						'connections'   => array( 'string' ),
					),		

				)
			)
		)
	)
));