<?php global $wp_embed; ?>
<style>
div.management ul {
  list-style: none;
  padding: 0;
}
div.management li {
  padding-left: 1.3em;
}
div.management li:before {
  content: "\f00c";
  font-family: "FontAwesome";
  display: inline-block;
  margin-left: -1.3em;
  width: 1.3em;
}
</style>
<div class="management fl-accordion fl-accordion-<?php echo $settings->label_size;
if ( $settings->collapse ) { echo ' fl-accordion-collapse';} ?>"  role="tablist"<?php if ( ! $settings->collapse ) { echo 'multiselectable="true"';} ?>>
	<?php for ( $i = 0; $i < count( $settings->management );
	$i++ ) : if ( empty( $settings->management[ $i ] ) ) { continue;} ?>
	<div class="fl-accordion-item"<?php if ( ! empty( $settings->id ) ) { echo ' id="' . sanitize_html_class( $settings->id ) . '-' . $i . '"';} ?>>
		<div class="fl-accordion-button" id="<?php echo 'fl-accordion-' . $module->node . '-tab-' . $i; ?>" aria-selected="false" aria-controls="<?php echo 'fl-accordion-' . $module->node . '-panel-' . $i; ?>" aria-expanded="<?php echo ( $i > 0 || ! $settings->open_first) ? 'false' : 'true'; ?>" role="tab" tabindex="0">
			<div class="fl-accordion-button-label"><?php echo $settings->management[ $i ]->mitarbeitername; ?></div>
			<i class="fl-accordion-button-icon fa fa-plus"></i>
		</div>
		<div class="fl-accordion-content fl-clearfix" id="<?php echo 'fl-accordion-' . $module->node . '-panel-' . $i; ?>" aria-labelledby="<?php echo 'fl-accordion-' . $module->node . '-tab-' . $i; ?>" aria-hidden="<?php echo ( $i > 0 || ! $settings->open_first) ? 'true' : 'false'; ?>" role="tabpanel" aria-live="polite">
			<div>
				<?php echo wpautop( $wp_embed->autoembed( $settings->management[ $i ]->stichpunkte ) ); ?>
			</div>
			<div style="padding-top:30px; padding-bottom:10px;">
				<?php echo wpautop( $wp_embed->autoembed( $settings->management[ $i ]->subtext ) ); ?>
			</div>			
		</div>
	</div>
	<?php endfor; ?>
</div>
