<?php

$fullsize_path_img1 = basename(get_attached_file($settings->photo));
$fullsize_path_img2 = basename(get_attached_file($settings->photo2));
$img1 = "/wp-content/uploads/" . $fullsize_path_img1;
$img2 = "/wp-content/uploads/" . $fullsize_path_img2;

$title = $settings->title;
$subtitle = $settings->subtitle;
?>
<style>


    .imgtitle,
    .maintitle {
        line-height: 1;
    }


    .imgtitle {
        position: absolute;
        bottom: -100px;
        font-size: 40px;
        text-transform: uppercase;
        color: #ffffff;
        z-index: 20;
        width: 30%;
        left: 20%;
        font-weight: bold;
    }

    .shape-t,
    .shape-t:after,
    .shape-t-img {
        width:100%;
        height:400px;
        transition: all 300ms;
    }

    .shape-t{
        width:70%;
    }

    .shape-t{
        -webkit-clip-path: polygon(0% 0%, 100% 0%, 80% 100%, 0% 100%);
    }

    .shape-t:after,
    .shape-t-img{
        -webkit-clip-path: polygon(
                calc(0% + 0px) 0%,
                calc(100% - 20px) 0%,
                calc(80% - 20px) 100%,
                calc(0% + 0px) 100%
        );
    }


    /* Weißer Rand */
    .shape-t {
        margin:0px auto;
        background-color:#ffffff;

        position:absolute;
        top:0px;

        z-index:10;
    }

    /* Einfärbung Bild */
    .shape-t:after{
        content:"";

        position:absolute;
        top:0px;

        background-color:rgba(255,55,0,.5);
    }


    /* Hintergrundbild */
    .shape-t-img {
        background-image:url(<?=$img1?>);
        background-size:cover;
    }


    .shape-t2 {
        width: 100%;
        height: 400px;
        transition: all 300ms;
    }

    .shape-t2 {
        width: 50%;
        right: 0px;
        position: absolute;
        background-image: url(<?=$img2?>);
        background-size: cover;
    }

    .imgtitle.banner {
        position: absolute;
        top: 80px;
        bottom: inherit;
        font-size: 30px;
        text-transform: uppercase;
        color: #ffffff;
        z-index: 20;
        width: 55%;
        left: 10%;
        font-weight: bold;
    }

    .imgsubtitle.banner {
        position: absolute;
        bottom: 80px;
        font-size: 20px;
        text-transform: uppercase;
        color: #ffffff;
        z-index: 20;
        width: 30%;
        left: 20%;
        font-weight: bold;
        text-align: right;
    }

    @media screen and (max-width: 1000px) {
        .shape-t,
        .shape-t-img,
        .shape-t:after {
            width: 100%;
            -webkit-clip-path: polygon(0% 0%, 100% 0%, 100% 100%, 0% 100%);

        }

        .shape-t2 {
            display: none;
        }

        .imgtitle.banner,
        .imgsubtitle.banner {
            width: 90%;
            left: 5%;
            font-size: 3.2vw;
        }

        .imgtitle.banner {
            font-size: 4vw;
        }

    }

</style>


<div style="position:relative; height:400px;">

    <div class="imgtitle banner">
        <?=$title?>
    </div>
    <div class="imgsubtitle banner">
        <?=$subtitle?>
    </div>


    <div class="shape-t">
        <div class="shape-t-img">
        </div>
    </div>


    <div class="shape-t2">
        <div class="shape-t2-img">
        </div>
    </div>

</div>


