<?php

/**
 * @class StellenanzeigeModule
 */
class StellenanzeigeModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('Stellennanzeige-Modul', 'fl-builder'),
			'description'   	=> __('Zeigt unterschiedliche Stellenanzeigen an.', 'fl-builder'),
			'category'      	=> __('Fuelcellpowertrain Module', 'fl-builder'),
			'partial_refresh'	=> true
		));
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('StellenanzeigeModule', array(
	'general'       => array(
		'title'         => __('Stellenanzeige', 'fl-builder'),
		'sections'      => array(
			'general'       => array(
				'title'         => '',
				'fields'        => array(				
					'stellenanzeige'        => array(
						'type'          => 'form',
						'label'         => __('Stellenanzeige', 'fl-builder'),
						'form'          => 'content_stellenanzeige', // ID from registered form below
						'preview_text'  => 'label', // Name of a field to use for the preview text
						'multiple'      => true
					)
				)
			)
		)
	),
	'style'        => array(
		'title'         => __( 'Style', 'fl-builder' ),
		'sections'      => array(
			'general'       => array(
				'title'         => '',
				'fields'        => array(
					'border_color'  => array(
						'type'          => 'color',
						'label'         => __( 'Border Color', 'fl-builder' ),
						'default'       => 'e5e5e5',
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.fl-accordion-item',
							'property'      => 'border-color',
						),
					),
					'label_size'   => array(
						'type'          => 'select',
						'label'         => __( 'Label Size', 'fl-builder' ),
						'default'       => 'small',
						'options'       => array(
							'small'         => _x( 'Small', 'Label size.', 'fl-builder' ),
							'medium'        => _x( 'Medium', 'Label size.', 'fl-builder' ),
							'large'         => _x( 'Large', 'Label size.', 'fl-builder' ),
						),
						'preview'       => array(
							'type'          => 'none',
						),
					),
					'item_spacing'     => array(
						'type'          => 'text',
						'label'         => __( 'Item Spacing', 'fl-builder' ),
						'default'       => '10',
						'maxlength'     => '2',
						'size'          => '3',
						'description'   => 'px',
						'sanitize'		=> 'absint',
						'preview'       => array(
							'type'          => 'none',
						),
					),
					'collapse'   => array(
						'type'          => 'select',
						'label'         => __( 'Collapse Inactive', 'fl-builder' ),
						'default'       => '1',
						'options'       => array(
							'1'             => __( 'Yes', 'fl-builder' ),
							'0'             => __( 'No', 'fl-builder' ),
						),
						'help'          => __( 'Choosing yes will keep only one item open at a time. Choosing no will allow multiple items to be open at the same time.', 'fl-builder' ),
						'preview'       => array(
							'type'          => 'none',
						),
					),
					'open_first'       => array(
						'type'          => 'select',
						'label'         => __( 'Expand First Item', 'fl-builder' ),
						'default'       => '0',
						'options'       => array(
							'0'             => __( 'No', 'fl-builder' ),
							'1'             => __( 'Yes', 'fl-builder' ),
						),
						'help' 			=> __( 'Choosing yes will expand the first item by default.', 'fl-builder' ),
					),
				),
			),
		),
	),
));

/**
 * Register the text settings form.
 */
FLBuilder::register_settings_form('content_stellenanzeige', array(
	'title' => __('Text Settings', 'fl-builder'),
	'tabs'  => array(
		'general'        => array( // Tab
			'title'         => __('General', 'fl-builder'), // Tab title
			'sections'      => array( // Tab Sections
				'general'       => array(
					'title'     => '',
					'fields'    => array(
						'label'         => array(
							'type'          => 'text',
							'label'         => __('Text Label', 'fl-builder'),
							'help'          => __('Ein Label um den Text zu identifizieren.', 'fl-builder')
						)
					)
				),
				'content'      => array(
					'title'         => __('Content Layout', 'fl-builder'),
					'fields'        => array(
						'title'          => array(
							'type'          => 'text',
							'label'         => __('Titel', 'fl-builder')
						),
						'introduction'          => array(
							'type'          => 'editor',
							'label'         => __('Einleitung', 'fl-builder'),
							'rows'          => 13,
							'wpautop'		=> false,
							'preview'         => array(
								'type'             => 'text',
								'selector'         => '.fl-rich-text'  
							)
						),
						'subtext'          => array(
							'type'          => 'editor',
							'label'         => __('Subtext', 'fl-builder'),
							'rows'          => 13,
							'wpautop'		=> false,
							'preview'         => array(
								'type'             => 'text',
								'selector'         => '.fl-rich-text'  
							)
						),
						'tasks'          => array(
							'type'          => 'editor',
							'label'         => __('Aufgaben', 'fl-builder'),
							'rows'          => 13,
							'wpautop'		=> false,
							'preview'         => array(
								'type'             => 'text',
								'selector'         => '.fl-rich-text'  
							)
						),
						'qualifications'          => array(
							'type'          => 'editor',
							'label'         => __('Qualifikation', 'fl-builder'),
							'rows'          => 13,
							'wpautop'		=> false,
							'preview'         => array(
								'type'             => 'text',
								'selector'         => '.fl-rich-text'  
							)
						),
						'we_offer'          => array(
							'type'          => 'editor',
							'label'         => __('Wir bieten', 'fl-builder'),
							'rows'          => 13,
							'wpautop'		=> false,
							'preview'         => array(
								'type'             => 'text',
								'selector'         => '.fl-rich-text'  
							)
						),
						'further_information'          => array(
							'type'          => 'editor',
							'label'         => __('Weitere Informationen', 'fl-builder'),
							'rows'          => 13,
							'wpautop'		=> false,
							'preview'         => array(
								'type'             => 'text',
								'selector'         => '.fl-rich-text'  
							)
						)
					)
				)
			)
		)
	)
));