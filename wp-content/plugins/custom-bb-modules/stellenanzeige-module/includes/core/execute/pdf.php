<?php

$einleitung = $_POST["introduction"];
$title = $_POST["title"];
$subtext = $_POST["subtext"];
$tasks = $_POST["tasks"];
$qualifications = $_POST["qualifications"];
$we_offer = $_POST["we_offer"];
$further_information = $_POST["further_information"];

$lang = $_POST["lang"];

/*
echo($einleitung);
echo($title);
echo($subtext);
echo($tasks);
echo($qualifications);
echo($we_offer);
echo($further_information);
*/

//echo($tasks);
//exit;
?><?php
//https://tcpdf.org/docs/
//============================================================+
// File name   : example_006.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 006 for TCPDF class
//               WriteHTML and RTL support
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML and RTL support
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
/*
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
*/
$pdf->SetMargins(0, 0, 0);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);


// set auto page breaks
//$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// disable auto-page-break
$pdf->SetAutoPageBreak(false, 0);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);

// add a page
$pdf->AddPage();


// set JPEG quality
$pdf->setJPEGQuality(100);

// Hintergrundbild
if($lang=="DE")
{
	$pdf->Image('images/top-banner.png', 0, 0, 210, 80, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
}
else if($lang=="EN")
{
	$pdf->Image('images/top-banner_en.png', 0, 0, 210, 80, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
}


$txt = "";
$pdf->MultiCell(210, 80, '[LEFT] '.$txt, 1, 'L', 1, 1, '', '', true);

// Wasserzeichen
$pdf->Image('images/watermark.png', 150, 237, 60, 60, 'PNG', '', '', false, 300, '', false, false, 0, false, false, false);

// Listeneinrückungen einstellen
$pdf->setListIndentWidth(4);

/*
<ul style="font-size:7pt;list-style-type:img|png|3|3|images/check.png">
	<li>Aufnehmen von Spezifikationen aus dem technischen Team und Umsetzen in Zuliefereranfragen<br/></li>
	<li>Identifizieren von alternativen Komponenten und Beschaffung von Testkomponenten für R&D<br/></li>
	<li>Einkauf von Testständen, Komponenten, Brennstoffzellen, Peripheriekomponenten wie Pumpen, Ventilen usw., Fertigungsanlagen und Dienstleistungen<br/></li>
	<li>Verantwortung für die komplette Abwicklung des Wareneinkaufs und regelmäßige Bewertung der Zulieferer<br/></li>
	<li>Verhandeln von Preisen und Sicherstellen der korrekten Abwicklung<br/></li>
</ul>
						
*/

$tasks = str_replace('<ul>', '<ul style="font-size:7pt;list-style-type:img|png|3|3|images/check.png">', $tasks);
$tasks = str_replace('</li>', '<br/></li>', $tasks);

$qualifications = str_replace('<ul>', '<ul style="font-size:7pt;list-style-type:img|png|3|3|images/check.png">', $qualifications);
$qualifications = str_replace('</li>', '<br/></li>', $qualifications);

$we_offer = str_replace('<ul>', '<ul style="font-size:7pt;list-style-type:img|png|3|3|images/check.png">', $we_offer);
$we_offer = str_replace('</li>', '<br/></li>', $we_offer);

$further_information = str_replace('<p>&nbsp;</p>', '', $further_information);


//$pdf->RoundedRect(x, y, w, h, r, round_corner = '1111', style = '', border_style = nil, fill_color = nil);
//$pdf->RoundedRect(0,100, 20, 20, 0, round_corner = '1111', style = '', border_style = "", fill_color = "");
//$pdf->RoundedRect(5, 200, 40, 30, 3.50, '1111', 'D');

$tasks_title = "Aufgabe";
$qualifications_title = "Qualifikation";
$we_offer_title = "Wir bieten";
if($lang=="EN")
{
	$tasks_title = "Your tasks";
	$qualifications_title = "Our requirements";
	$we_offer_title = "We offer";
}


// create some HTML content
$html =
<<<EOF
<!-- EXAMPLE OF CSS STYLE -->
<style>
	*{
		color:#666666;
		font-size:90%;
	}

    h1 {
        color: navy;
        font-family: times;
        font-size: 24pt;
        text-decoration: underline;
		position:absolute;
		margin-top:100mm;
    }
	
	h2{
		font-size:120%;
	}
	
	h3{
		color:#4f9fa6;
	}
	
	div.rounded{
		border:1px solid red;
		border-radius:10px;
	}
	
	p.infocaption, span.infocaption{
		text-transform: uppercase;
		color:#4f9fa6;
		font-size:120%;		
	}
	
	ul, li{
		margin:0;
		padding:0;
	}
</style>

<table width="100%">
	<tr valign="bottom">
		<td width="5%"></td>
		<td rowspan="1" align="left" valign="middle" width="90%">
			<span style="text-align:justify;">$einleitung</span>
			<h2>$title</h2>
			<img src="images/h2sub.png" width="70" height="3" border="0" /><br/>
			<span style="text-align:justify;">$subtext</span><br/>
		</td>
	</tr>
	<tr valign="bottom">
		<td width="5%"></td>
		<td rowspan="1" align="left" valign="middle" width="90%">
			<table width="100%">
				<tr>
					<td rowspan="1" align="left" valign="middle" width="33%">
						<p class="infocaption">$tasks_title</p>
						$tasks
					</td>
					<td rowspan="1" align="left" valign="middle" width="33%">
						<p class="infocaption">$qualifications_title</p>
						$qualifications
					</td>
					<td rowspan="1" align="left" valign="middle" width="33%">
						<p class="infocaption">$we_offer_title</p>
						$we_offer
					</td>
				</tr>
			</table>
			
		</td>
	</tr>
	<tr valign="bottom">
		<td width="5%"></td>
		<td rowspan="1" align="left" valign="middle" width="50%">
			<span style="text-align:justify;">$further_information
			</span><br/>
		</td>
	</tr>
</table>
EOF;

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// ---------------------------------------------------------

$pdf_title = "Stellenangebot";
if($lang=="EN")
{
	$pdf_title = "Job offer";
}


//Close and output PDF document
//$pdf->Output('stellenangebot.pdf', 'I');
$pdf->Output($pdf_title.' '.$title.'.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+