<?php global $wp_embed; ?>
<style>
.pdfdownload{
	text-transform: uppercase;
	border: 1px solid #4f9fa6;
	border-radius:6px;
	padding:10px 15px;
	margin:0px 0px 15px 0px;
	display:inline-block;
	cursor:pointer;
}

.pdfdownload:after{
	font-family:"FontAwesome";
	content:"\f019";
	margin-left:10px;
}
</style>
<script>
$( document ).ready(function() {
    
	jQuery("a.pdfdownload").click(function(){
		var parent = jQuery(this).parent(".fl-accordion-content");
		//console.log(jQuery(parent).find("[data-stellenanzeige-t2]").html());
		
		// Daten setzen
		jQuery("form[name=pdfform] [name=introduction]").val(jQuery(parent).find("[data-stellenanzeige-t1]").html());
		jQuery("form[name=pdfform] [name=title]").val(jQuery(parent).find("[data-stellenanzeige-t2]").html());
		jQuery("form[name=pdfform] [name=subtext]").val(jQuery(parent).find("[data-stellenanzeige-t3]").html());
		jQuery("form[name=pdfform] [name=tasks]").val(jQuery(parent).find("[data-stellenanzeige-t4]").html());
		jQuery("form[name=pdfform] [name=qualifications]").val(jQuery(parent).find("[data-stellenanzeige-t5]").html());
		jQuery("form[name=pdfform] [name=we_offer]").val(jQuery(parent).find("[data-stellenanzeige-t6]").html());
		jQuery("form[name=pdfform] [name=further_information]").val(jQuery(parent).find("[data-stellenanzeige-t7]").html());
		console.log(jQuery(parent).find("[data-stellenanzeige-t4]").html());
		jQuery("form[name=pdfform]").submit();
	});

    jQuery(".fl-accordion-item .fl-accordion-button").click();
	
});
</script>

<form name="pdfform" action="/wp-content/plugins/custom-bb-modules/stellenanzeige-module/includes/core/execute/pdf.php" method="post" target="_blank">
    <input type="hidden" name="introduction" value="" />
	<input type="hidden" name="title" value="" />
	<input type="hidden" name="subtext" value="" />
	<input type="hidden" name="tasks" value="" />
	<input type="hidden" name="qualifications" value="" />
	<input type="hidden" name="we_offer" value="" />
	<input type="hidden" name="further_information" value="" />
	<?php
		$download_title = "Stellenanzeige als PDF";
		if(ICL_LANGUAGE_CODE == "en")
		{
			$download_title = "Download PDF";
		?>
			<input type="hidden" name="lang" value="EN" />
		<?php		
		}
		else
		{
		?>
			<input type="hidden" name="lang" value="DE" />
		<?php
		}
	?>
</form>

<div class="fl-accordion fl-accordion-<?php echo $settings->label_size;
if ( $settings->collapse ) { echo ' fl-accordion-collapse';} ?>"  role="tablist"<?php if ( ! $settings->collapse ) { echo 'multiselectable="true"';} ?>>
	<?php for ( $i = 0; $i < count( $settings->stellenanzeige );
	$i++ ) : if ( empty( $settings->stellenanzeige[ $i ] ) ) { continue;} ?>
	<div class="fl-accordion-item"<?php if ( ! empty( $settings->id ) ) { echo ' id="' . sanitize_html_class( $settings->id ) . '-' . $i . '"';} ?>>
		<div class="fl-accordion-button" id="<?php echo 'fl-accordion-' . $module->node . '-tab-' . $i; ?>" aria-selected="false" aria-controls="<?php echo 'fl-accordion-' . $module->node . '-panel-' . $i; ?>" aria-expanded="<?php echo ( $i > 0 || ! $settings->open_first) ? 'false' : 'true'; ?>" role="tab" tabindex="0">
			<div class="fl-accordion-button-label"><?php echo $settings->stellenanzeige[ $i ]->title; ?></div>
			<i class="fl-accordion-button-icon fa fa-plus"></i>
		</div>
		<div class="fl-accordion-content fl-clearfix" id="<?php echo 'fl-accordion-' . $module->node . '-panel-' . $i; ?>" aria-labelledby="<?php echo 'fl-accordion-' . $module->node . '-tab-' . $i; ?>" aria-hidden="<?php echo ( $i > 0 || ! $settings->open_first) ? 'true' : 'false'; ?>" role="tabpanel" aria-live="polite">
			<div data-stellenanzeige-t1="1">
				<?php echo wpautop( $wp_embed->autoembed( $settings->stellenanzeige[ $i ]->introduction ) ); ?>
			</div>
			<?php
			if($settings->stellenanzeige[ $i ]->tasks != "")
			{
			?>
			<div class="jobs_title">
				<h2 data-stellenanzeige-t2="1"><?php echo $settings->stellenanzeige[ $i ]->title; ?></h2>
			</div>
			<?php
			}
			?>
			<div data-stellenanzeige-t3="1">
				<?php echo wpautop( $wp_embed->autoembed( $settings->stellenanzeige[ $i ]->subtext ) ); ?>
			</div>
			
			<?php
			if($settings->stellenanzeige[ $i ]->tasks != "")
			{
			?>
			<div class="jobs_stats_container">
				<div>
					<?php
						if(ICL_LANGUAGE_CODE == "en")
						{
					?>
							<p>Your Tasks</p>
					<?php
						}
						else
						{
					?>
							<p>Aufgaben</p>
					<?php
						}
					?>
					<span data-stellenanzeige-t4="1">
					<?php echo wpautop( $wp_embed->autoembed( $settings->stellenanzeige[ $i ]->tasks ) ); ?>
					</span>
				</div>
				<div>
					<?php
						if(ICL_LANGUAGE_CODE == "en")
						{
					?>
							<p>Our Requirements</p>
					<?php
						}
						else
						{
					?>
							<p>Qualifikation</p>
					<?php
						}
					?>
					<span data-stellenanzeige-t5="1">
					<?php echo wpautop( $wp_embed->autoembed( $settings->stellenanzeige[ $i ]->qualifications ) ); ?>
					</span>
				</div>
				<div>
					<?php
						if(ICL_LANGUAGE_CODE == "en")
						{
					?>
							<p>We offer</p>
					<?php
						}
						else
						{
					?>
							<p>Wir bieten</p>
					<?php
						}
					?>
					<span data-stellenanzeige-t6="1">
					<?php echo wpautop( $wp_embed->autoembed( $settings->stellenanzeige[ $i ]->we_offer ) ); ?>
					</span>
				</div>
			</div>
			<?php
			}
			?>
			
			<div class="jobs_further_information" data-stellenanzeige-t7="1">
				<?php echo wpautop( $wp_embed->autoembed( $settings->stellenanzeige[ $i ]->further_information ) ); ?>
			</div>
			
			<?php
			if(1==2)//$settings->stellenanzeige[ $i ]->tasks != "")
			{
			?>
			<a class="button pdfdownload"><?=$download_title?></a>
			<?php
			}
			?>
			
		</div>
	</div>
	<?php endfor; ?>
</div>

<script>
    jQuery( document ).ready(function() {

        setTimeout(function(){
            jQuery(".fl-accordion-item .fl-accordion-button").click();
        }, 500);



    });
</script>