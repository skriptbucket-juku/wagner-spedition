<?php global $wp_embed; ?>
<style>


.history-module .new_row{
	display:flex;
}

.history-module .new_row .row-col{
	width:calc((100% - 200px) / 2);
	border:0px solid black;
}
.history-module .new_row .row-col.row-col-2{
	width:200px;
	text-align:center;
	position:relative;
	min-height:120px;
}

.history-module .new_row.endings .row-col.row-col-2{
	min-height:40px;
}

.history-module .new_row .row-col.row-col-2 span{
	margin:0px auto;
	background-color:#ffffff;
	border:5px solid #a0a0a0;
	color:#a0a0a0;
	border-radius: 50%;
	width:80px;
	height:80px;
	line-height:73px;
	box-sizing:border-box;
	display:inline-block;
	z-index:10;
	position:relative;
	box-shadow:2px 3px 6px #333333;
}

/*balken nach rechts*/
.history-module .new_row.row_left .row-col.row-col-2 span:before{
    content: "";
    display: block;
    left: -50px;
    top: 34px;
    position: absolute;
    background-color: #a0a0a0;
    width: 50px;
    height: 4px;
}

/*balken nach links*/
.history-module .new_row.row_right .row-col.row-col-2 span:before{
    content: "";
    display: block;
    right: -50px;
    top: 34px;
    position: absolute;
    background-color: #a0a0a0;
    width: 50px;
    height: 4px;
}


.history-module .new_row .row-col.row-col-2:before{
	content:"";
	position:absolute;
	left:95px; /*(200px - 10) / 2*/
	background-color: #a0a0a0;
	width:10px;
	height: 100%;
}

.history-module .new_row .row-col.row-col-1{
	text-align:right;
}

@media screen and (max-width:767px)
{
	.history-module .new_row{
		flex-direction: column;
	}
	
	.history-module .new_row .row-col{
		width:100%!important;
		text-align:center!important;
	}
	
	.history-module .new_row .row-col.row-col-2:before{
		left: calc(50% - 5px);
	}	

	.history-module .new_row.row_left .row-col.row-col-2 span:before,
	.history-module .new_row.row_right .row-col.row-col-2 span:before{
		display:none;
	}
	
	/* linke seite */
	.history-module .new_row.row_left .row-col-1{
		order:2;
	}
	.history-module .new_row.row_left .row-col-2{
		order:1;
	}
	.history-module .new_row.row_left .row-col-3{
		display:none;
	}
	
	.realend{
		display:none!important;
	}
	
	.history-module .new_row .row-col{
		margin:5px 0px;
	}
	
	.history-module .new_row .row-col.row-col-2 {
		min-height: 140px;
	}
	
	.history-module .new_row .row-col.row-col-2 span{
		margin-top:30px;
		
	}
}

</style>

<div class="history-module">

<div class="new_row endings">
	<div class="row-col row-col-1"></div><div class="row-col row-col-2"></div><div class="row-col row-col-3"></div>
</div>

<?php 
$display = 0;
for ( $i = 0; $i < count( $settings->historysteps ); $i++)
{

	if ( empty( $settings->historysteps[ $i ] ) ) { continue;} 
	
	$display++;
	
	$rowDisplay = "row_left";
	if($display%2 == 1)
	{
		$rowDisplay = "row_right";
	}
	
	// Neue Zeile
	echo('<div class="new_row '.$rowDisplay.'">'); // #1
		
	if($display%2 == 1)
	{
		// rechts
		?>
			<div class="row-col row-col-1"></div>
			<div class="row-col row-col-2"><span><?=$settings->historysteps[ $i ]->year?></span></div>
			<div class="row-col row-col-3">
				<?=wpautop( $wp_embed->autoembed( $settings->historysteps[ $i ]->desc ) )?>
			</div>
		<?php
	}
	else
	{
		// links
		?>
			<div class="row-col row-col-1">
				<?=wpautop( $wp_embed->autoembed( $settings->historysteps[ $i ]->desc ) )?>
			</div>
			<div class="row-col row-col-2"><span><?=$settings->historysteps[ $i ]->year?></span></div>
			<div class="row-col row-col-3"></div>
		<?php
	}
	echo('</div>'); // #1
}
?>

<div class="new_row endings realend">
	<div class="row-col row-col-1"></div><div class="row-col row-col-2"></div><div class="row-col row-col-3"></div>
</div>

</div>