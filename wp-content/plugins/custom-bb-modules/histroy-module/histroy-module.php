<?php

/**
 * @class StellenanzeigeModule
 */
class HistoryModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('HistoryModule', 'fl-builder'),
			'description'   	=> __('Zeigt unterschiedliche Stellenanzeigen an.', 'fl-builder'),
			'category'      	=> __('Wagner-Module', 'fl-builder'),
			'partial_refresh'	=> true
		));
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('HistoryModule', array(
	'general'       => array(
		'title'         => __('Firmengeschichte', 'fl-builder'),
		'sections'      => array(
			'general'       => array(
				'title'         => '',
				'fields'        => array(				
					'historysteps'        => array(
						'type'          => 'form',
						'label'         => __('Unterpunkt', 'fl-builder'),
						'form'          => 'content_history', // ID from registered form below
						'preview_text'  => 'label', // Name of a field to use for the preview text
						'multiple'      => true
					)
				)
			)
		)
	),
	'style'        => array(
		'title'         => __( 'Style', 'fl-builder' ),
		'sections'      => array(
			'general'       => array(
				'title'         => '',
				'fields'        => array(
					'border_color'  => array(
						'type'          => 'color',
						'label'         => __( 'Border Color', 'fl-builder' ),
						'default'       => 'e5e5e5',
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.fl-accordion-item',
							'property'      => 'border-color',
						),
					),
					'label_size'   => array(
						'type'          => 'select',
						'label'         => __( 'Label Size', 'fl-builder' ),
						'default'       => 'small',
						'options'       => array(
							'small'         => _x( 'Small', 'Label size.', 'fl-builder' ),
							'medium'        => _x( 'Medium', 'Label size.', 'fl-builder' ),
							'large'         => _x( 'Large', 'Label size.', 'fl-builder' ),
						),
						'preview'       => array(
							'type'          => 'none',
						),
					),
					'item_spacing'     => array(
						'type'          => 'text',
						'label'         => __( 'Item Spacing', 'fl-builder' ),
						'default'       => '10',
						'maxlength'     => '2',
						'size'          => '3',
						'description'   => 'px',
						'sanitize'		=> 'absint',
						'preview'       => array(
							'type'          => 'none',
						),
					),
					'collapse'   => array(
						'type'          => 'select',
						'label'         => __( 'Collapse Inactive', 'fl-builder' ),
						'default'       => '1',
						'options'       => array(
							'1'             => __( 'Yes', 'fl-builder' ),
							'0'             => __( 'No', 'fl-builder' ),
						),
						'help'          => __( 'Choosing yes will keep only one item open at a time. Choosing no will allow multiple items to be open at the same time.', 'fl-builder' ),
						'preview'       => array(
							'type'          => 'none',
						),
					),
					'open_first'       => array(
						'type'          => 'select',
						'label'         => __( 'Expand First Item', 'fl-builder' ),
						'default'       => '0',
						'options'       => array(
							'0'             => __( 'No', 'fl-builder' ),
							'1'             => __( 'Yes', 'fl-builder' ),
						),
						'help' 			=> __( 'Choosing yes will expand the first item by default.', 'fl-builder' ),
					),
				),
			),
		),
	),
));

/**
 * Register the text settings form.
 */
FLBuilder::register_settings_form('content_history', array(
	'title' => __('Text Settings', 'fl-builder'),
	'tabs'  => array(
		'general'        => array( // Tab
			'title'         => __('General', 'fl-builder'), // Tab title
			'sections'      => array( // Tab Sections
				'general'       => array(
					'title'     => '',
					'fields'    => array(
						'label'         => array(
							'type'          => 'text',
							'label'         => __('Text Label', 'fl-builder'),
							'help'          => __('Ein Label um den Text zu identifizieren.', 'fl-builder')
						)
					)
				),
				'content'      => array(
					'title'         => __('Content Layout', 'fl-builder'),
					'fields'        => array(
						'year'          => array(
							'type'          => 'text',
							'label'         => 'Jahreszahl'
						),
						'desc'          => array(
							'type'          => 'editor',
							'label'         => 'Beschreibung',
							'rows'          => 13,
							'wpautop'		=> false,
							'preview'         => array(
								'type'             => 'text',
								'selector'         => '.fl-rich-text'  
							)
						),
					)
				)
			)
		)
	)
));