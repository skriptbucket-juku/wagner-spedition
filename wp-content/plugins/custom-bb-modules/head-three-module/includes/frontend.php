<?php


$fullsize_path_img1 = basename(get_attached_file($settings->photo));
$fullsize_path_img2 = basename(get_attached_file($settings->photo2));
$fullsize_path_img3 = basename(get_attached_file($settings->photo3));

//echo("titel:".$settings->title);
//echo("<br/>titel:".$fullsize_path_img1);
//echo("<br/>titel:".$fullsize_path_img2);
//echo("<br/>titel:".$fullsize_path_img3);
//<img src="/wp-content/uploads/<?=$fullsize_path_img1?_>" />

?>

<style>


    .outer-header {
        margin-bottom: 200px;
    }

    .outer-header,
    .shape-a,
    .shape-c {
        height: 400px;

    }

    .inner-header {
        max-width: 1000px;
        margin: 0px auto;
        height: 100%;
        position: relative;
    }

    .shape-b,
    .shape-b:after,
    .shape-b-img {
        width: 1000px;
        height: 540px;
        transition: all 300ms;
    }

    #navigation {
        position: absolute;
        right: 75px;
        z-index: 999;
        margin-top: 40px;
    }

    #navigation span {
        padding: 3px 10px;
        color: #ffffff;
        font-weight: bold;
    }

    #navigation span:hover {
        color: #ffffff;
        border-top: 2px solid #ffffff;
    }

    .shape-b {
        -webkit-clip-path: polygon(45% 0%, 100% 0%, 55% 100%, 0% 100%);
    }

    .shape-b:after,
    .shape-b-img {
        -webkit-clip-path: polygon(
                calc(45% + 20px) 0%,
                calc(100% - 20px) 0%,
                calc(55% - 20px) 100%,
                calc(0% + 20px) 100%
        );
    }

    /* Weißer Ranb */
    .shape-b {
        margin: 0px auto;
        background-color: #ffffff;

        position: absolute;
        top: 0px;

        z-index: 10;
    }

    /* Einfärbung Bild */
    .shape-b:after {
        content: "";

        position: absolute;
        top: 0px;

        background-color: rgba(255, 55, 0, .5);
    }

    /* Hintergrundbild */
    .shape-b-img {
        background-image: url(/wp-content/uploads/<?=$fullsize_path_img2?>);
        background-size: cover;
    }

    .shape-a,
    .shape-c {
        width: 50%;

        position: absolute;
        top: 0px;

        background-position: center;
        background-size: cover;
    }

    .shape-a {
        left: 0px;
        background-image: url(/wp-content/uploads/<?=$fullsize_path_img1?>);
    }

    .shape-c {
        right: 0px;
        background-image: url(/wp-content/uploads/<?=$fullsize_path_img3?>);

    }

    .imgtitle,
    .maintitle {
        line-height: 1;
    }


    .imgtitle {
        position: absolute;
        bottom: -100px;
        font-size: 40px;
        text-transform: uppercase;
        color: #ffffff;
        z-index: 20;
        width: 30%;
        left: 20%;
        font-weight: bold;
    }

    .maintitle {
        position: absolute;
        bottom: -200px;
        font-size: 40px;
        text-transform: uppercase;
        color: #000000;
        z-index: 20;
        width: 22%;
        left: 63%;
        border-top: 5px solid #000000;
        font-weight: bold;
        text-align: right;
    }

    @media screen and (max-width: 1000px) {
        .shape-a, .shape-c {
            display: none;
        }

        .shape-b,
        .shape-b-img,
        .shape-b:after {
            -webkit-clip-path: polygon(0% 0%, 100% 0%, 100% 100%, 0% 100%);
            height: 200px;
            background-position: center;
        }

        .outer-header,
        .shape-b,
        .shape-b-img,
        .shape-b:after {
            height: 200px;
            width: 100%;
        }

        .imgtitle {
            font-size: 30px;
            width: 100%!important;
            bottom: 20px!important;
            left: 0px!important;
            text-align: center;
            padding: 20px;
            box-sizing: border-box;
        }

        .maintitle {
            left: 0%;
            padding: 20px;
        }
    }


    /* Animation */

    .shape-b-img {
        animation-name: iliketomoveit;
        animation-duration: 15s;
        animation-iteration-count: infinite;

        background-size: 120%;
    }


</style>


<div class="outer-header">

    <div class="shape-a">
    </div>

    <div class="inner-header">
        <!--
        <div id="navigation">
            <span>Aktuelles</span>
            <span>Unternehmen</span>
            <span>Service</span>
            <span>Fuhrpark</span>
            <span>Karriere</span>
        </div>
        -->
        <div class="imgtitle">
            <?= $settings->title ?>
        </div>

        <div class="shape-b">
            <div class="shape-b-img">
            </div>
        </div>

        <div class="maintitle">
            Wagner Spedition Service
        </div>

    </div>

    <div class="shape-c">
    </div>
</div>



