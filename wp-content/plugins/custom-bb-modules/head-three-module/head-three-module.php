<?php

/**
 * @class TextImageModule
 */
class HeadThreeModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('HeadThreeModule', 'fl-builder'),
			'description'   	=> __('Displays multiple slides with an optional heading and call to action.', 'fl-builder'),
			'category'      	=> __('Wagner-Module', 'fl-builder'),
			'partial_refresh'	=> true
		));

		$this->add_css('jquery-bxslider');
		$this->add_js('jquery-bxslider');
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('HeadThreeModule', array(
	'general'       => array(
		'title'         => __('General', 'fl-builder'),
		'sections'      => array(
			'general'       => array(
				'title'         => '',
				'fields'        => array(
				
				
				
				
				
				
					'photo_source'  => array(
						'type'          => 'select',
						'label'         => __('Foto links', 'fl-builder'),
						'default'       => 'library',
						'options'       => array(
							'library'       => __('Media Library', 'fl-builder'),
							'url'           => __('URL', 'fl-builder')
						),
						'toggle'        => array(
							'library'       => array(
								'fields'        => array('photo')
							),
							'url'           => array(
								'fields'        => array('photo_url', 'caption')
							)
						)
					),
					
					'photo'         => array(
						'type'          => 'photo',
						'label'         => __('Photo', 'fl-builder')
					),
					
					'photo_url'     => array(
						'type'          => 'text',
						'label'         => __('Photo URL', 'fl-builder'),
						'placeholder'   => __( 'http://www.example.com/my-photo.jpg', 'fl-builder' )
					),
					
					
					
					
					
					
					
					
					'photo_source2'  => array(
						'type'          => 'select',
						'label'         => __('Foto mitte', 'fl-builder'),
						'default'       => 'library',
						'options'       => array(
							'library'       => __('Media Library', 'fl-builder'),
							'url'           => __('URL', 'fl-builder')
						),
						'toggle'        => array(
							'library'       => array(
								'fields'        => array('photo2')
							),
							'url'           => array(
								'fields'        => array('photo_url2', 'caption')
							)
						)
					),
					
					'photo2'         => array(
						'type'          => 'photo',
						'label'         => __('Photo', 'fl-builder')
					),
					
					'photo_url2'     => array(
						'type'          => 'text',
						'label'         => __('Photo URL', 'fl-builder'),
						'placeholder'   => __( 'http://www.example.com/my-photo.jpg', 'fl-builder' )
					),
					
					
					
					
					
					'photo_source3'  => array(
						'type'          => 'select',
						'label'         => __('Foto rechts', 'fl-builder'),
						'default'       => 'library',
						'options'       => array(
							'library'       => __('Media Library', 'fl-builder'),
							'url'           => __('URL', 'fl-builder')
						),
						'toggle'        => array(
							'library'       => array(
								'fields'        => array('photo3')
							),
							'url'           => array(
								'fields'        => array('photo_url3', 'caption')
							)
						)
					),
					
					'photo3'         => array(
						'type'          => 'photo',
						'label'         => __('Photo', 'fl-builder')
					),
					
					'photo_url3'     => array(
						'type'          => 'text',
						'label'         => __('Photo URL', 'fl-builder'),
						'placeholder'   => __( 'http://www.example.com/my-photo.jpg', 'fl-builder' )
					),
					
					
					
					
					'title'          => array(
						'type'          => 'editor',
						'label'         => 'Titel',
						'rows'          => 2,
						'wpautop'		=> false,
						'preview'         => array(
							'type'             => 'text',
							'selector'         => '.fl-rich-text',
						),
						'connections'   => array( 'string' ),
					),				
					
			
				)
			)
		)
	)
));