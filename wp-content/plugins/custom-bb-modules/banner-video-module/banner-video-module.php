<?php

/**
 * @class BannerVideoModule
 */
class BannerVideoModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('BannerVideoModule', 'fl-builder'),
			'description'   	=> __('Banner mit zwei Grafiken (rot/normal)', 'fl-builder'),
			'category'      	=> __('Wagner-Module', 'fl-builder'),
			'partial_refresh'	=> true
		));

		$this->add_css('jquery-bxslider');
		$this->add_js('jquery-bxslider');
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('BannerVideoModule', array(
	'general'       => array(
		'title'         => __('General', 'fl-builder'),
		'sections'      => array(
			'general'       => array(
				'title'         => '',
				'fields'        => array(
				
				
				
				
				
				
					'photo_source'  => array(
						'type'          => 'select',
						'label'         => __('Foto links', 'fl-builder'),
						'default'       => 'library',
						'options'       => array(
							'library'       => __('Media Library', 'fl-builder'),
							'url'           => __('URL', 'fl-builder')
						),
						'toggle'        => array(
							'library'       => array(
								'fields'        => array('photo')
							),
							'url'           => array(
								'fields'        => array('photo_url', 'caption')
							)
						)
					),
					
					'photo'         => array(
						'type'          => 'photo',
						'label'         => __('Photo', 'fl-builder')
					),
					
					'photo_url'     => array(
						'type'          => 'text',
						'label'         => __('Photo URL', 'fl-builder'),
						'placeholder'   => __( 'http://www.example.com/my-photo.jpg', 'fl-builder' )
					),
					
					
					
					
					
					'videourl'     => array(
						'type'          => 'text',
						'label'         => 'URL zum Video (intern oder extern)',
						'placeholder'   => 'https://'
					),
								
					
					
			
				)
			)
		)
	)
));