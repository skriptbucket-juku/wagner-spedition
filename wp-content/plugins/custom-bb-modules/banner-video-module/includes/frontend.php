<?php

$fullsize_path_img1 = basename(get_attached_file($settings->photo));
$img1 = "/wp-content/uploads/" . $fullsize_path_img1;

$videourl = $settings->videourl;

?>
<style>


.rel-container{
	position:relative;
	margin:40px 0px;
	display: flex;

    align-items: center;
    justify-content: center;
    
	height: 60vw;
	max-height:800px;

}

.rel-container .shape-video{

	height:100%;
	width:100%;

	position:absolute;
	left:0px;
	top:0px;
		
	background-position:center;
	background-size:cover;

	background-image:url(<?=$img1?>);
	
	
	-webkit-clip-path: polygon(0% 0%, 80% 0%, 60% 100%, 0% 100%);	
}


.rel-container .shape-video-content{
	position:absolute;
	background-color:grey;
	border:20px solid #ffffff;
	margin:0px auto;
	
	
	width:60vw;
	max-width:1200px;
	
	height:calc(60vw / 16 * 9);
	/*max-height:600px;*/
}

/*
.rel-container .shape-video-content:after{
	content:"";
	display:block;
	width:100%;
	height:100%;
	background-color:rgba(255,0,0,.5);
}
*/

.shape-video-content video{
	width: 100%;
    height: calc(60vw / 16 * 9 - 40px);
    background-color: #000000;	
}

@media screen and (max-width:1000px)
{
	.rel-container .shape-video-content{
		border:5px solid #ffffff;
	}
	
	.shape-video-content video{
		height: calc(60vw / 16 * 9 - 10px);
	}
	
	
	.rel-container .shape-video{
		-webkit-clip-path: polygon(0% 0%, 100% 0%, 100% 100%, 0% 100%);	
	}

	
}




</style>


<div class="rel-container">
	<div class="shape-video"></div>
	<div class="shape-video-content">
		<video src="http://wiki.selfhtml.org/local/small.mp4" controls>
			Ihr Browser kann dieses Video nicht wiedergeben.
		</video>
</main>
	</div>
</div>


