<?php

/**
 * @class BlogLikePostsModule
 */
class BlogLikePostsModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('BlogLikePostsModule', 'fl-builder'),
			'description'   	=> __('Blogbeiträge in Kacheln mit Bildern darstellen', 'fl-builder'),
			'category'      	=> __('Wagner-Module', 'fl-builder'),
			'partial_refresh'	=> true
		));

		$this->add_css('jquery-bxslider');
		$this->add_js('jquery-bxslider');
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('BlogLikePostsModule', array(
	'general'       => array(
		'title'         => 'Allgemein',
		'sections'      => array(
			'general'       => array(
				'title'         => '',
				'fields'        => array(
				
					'modulsubtext'         => array(
							'type'          => 'text',
							'label'         => 'Gesamtüberschrift',
							'help'          => __('', 'fl-builder')
						),
				)
			)
		)
	),
	'pages'       => array(
		'title'         => 'Einträge',
		'sections'      => array(
			'general'       => array(
				'title'         => '',
				'fields'        => array(				
				
					'pages'        => array(
						'type'          => 'form',
						'label'         => 'Eintrag',
						'form'          => 'bloglikepostsmodule_entries', // ID from registered form below
						'preview_text'  => 'label', // Name of a field to use for the preview text
						'multiple'      => true
					)
				)
			)
		)
	)
));

/**
 * Register the text settings form.
 */
FLBuilder::register_settings_form('bloglikepostsmodule_entries', array(
	'title' => __('Text Settings', 'fl-builder'),
	'tabs'  => array(
		'general'        => array( // Tab
			'title'         => __('General', 'fl-builder'), // Tab title
			'sections'      => array( // Tab Sections
				'general'       => array(
					'title'     => '',
					'fields'    => array(
					
					
						'title'         => array(
							'type'          => 'text',
							'label'         => __('Überschrift', 'fl-builder'),
							'help'          => __('', 'fl-builder')
						),
						
						'text'          => array(
							'type'          => 'editor',
							'label'         => 'Beschreibung',
							'rows'          => 13,
							'wpautop'		=> false,
							'preview'         => array(
								'type'             => 'text',
								'selector'         => '.fl-rich-text'  
							)
						),				
						
					)
				),
				'content'      => array(
					'title'         => __('Hintergrundbild', 'fl-builder'),
					'fields'        => array(
											
						'photo_source'  => array(
							'type'          => 'select',
							'label'         => __('Photo Source', 'fl-builder'),
							'default'       => 'library',
							'options'       => array(
								'library'       => __('Media Library', 'fl-builder'),
								'url'           => __('URL', 'fl-builder')
							),
							'toggle'        => array(
								'library'       => array(
									'fields'        => array('photo')
								),
								'url'           => array(
									'fields'        => array('photo_url', 'caption')
								)
							)
						),
						
						'photo'         => array(
							'type'          => 'photo',
							'label'         => __('Photo', 'fl-builder')
						),
						
						'photo_url'     => array(
							'type'          => 'text',
							'label'         => __('Photo URL', 'fl-builder'),
							'placeholder'   => __( 'http://www.example.com/my-photo.jpg', 'fl-builder' )
						),
								
					)
				)
			)
		)
	)
));