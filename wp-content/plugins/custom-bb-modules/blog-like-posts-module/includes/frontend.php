<?php
$anzahl = count($settings->pages);
if($anzahl > 0)
{
	?>
	<style>
	.blog .img{
		width:100%;
		height:300px;
		background-size:cover;
		background-position:center;
	}

	.blog .title{
		border-top:2px solid #333333;
		display:inline-block;
		margin:20px 0px 10px 0px;
		padding-right:20px;
		text-transform:uppercase;
		font-weight:bold;
		font-size:120%;
	}

	.blog .desc{
		text-align:justify;
	}	
	</style>	
	<?php
	
	// Gesamtüberschrift
	$sub_text = $settings->modulsubtext;
	if($sub_text != "")
	{
		echo("<h2>".$sub_text."</h2>");
	}

	echo('<div class="content my-0">');//#1
	$validEntry = 0;

	for($i = 0; $i < count($settings->pages); $i++)
	{
		
		if(!is_object($settings->pages[$i]))
		{
			continue;
		}
		else
		{
			$page = $settings->pages[$i];
			$validEntry++;
		}
		if($validEntry%2 == 1)
		{
			echo('<div class="row mx-0 blog">');//#2
		}
		

		// Überschrift
		$title = $page->title;
		// Beschreibungstext
		$text = $page->text;
		// Bildpfad
		$bild = $page->photo_src;
		
		?>
		<div class="col-md-6 my-5">
			<div class="img" style="background-image:url(<?=$bild?>);"></div>
			<div class="title"><?=$title?></div>
			<div class="desc">
				<?=$text?>
			</div>
		</div>
		<?php


		if($validEntry%2 == 0)
		{
			echo('</div>');//#2
		}
	}
	echo('</div>');//#1
}
?>