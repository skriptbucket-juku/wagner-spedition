<?php


$fullsize_path_img1 = basename(get_attached_file($settings->photo));
$fullsize_path_img2 = basename(get_attached_file($settings->photo2));


$img1 = "/wp-content/uploads/" . $fullsize_path_img1;
$img2 = "/wp-content/uploads/" . $fullsize_path_img2;

$title = $settings->title;
$subtitle = $settings->subtitle;

?>

<style>

.outer-header {
	margin-bottom: 200px;
	height: 400px;
}

.inner-header {
	max-width: 1000px;
	margin: 0px auto;
	height: 100%;
	position: relative;
}

/* ------------------------------------------------- */
/* Linke Bildbox */
.shape-a3-img{
	height: 400px;
	width:100%;
	position:absolute;

	background-image:url(<?=$img1?>);	
	background-size:cover;
	background-position:center;
}

/* Abdunklung */
.shape-a3-img:after{
	content:"";
	position:absolute;
	top:0px;
	width:100%;
	height:400px;
	background-color:rgba(0,0,0,.6);
}

/* ------------------------------------------------- */
/* Rechte Bildbox */
.shape-c3{
	height: 540px;
	width: calc((100% - 1000px) / 2 + 460px);
	position:absolute;
	right:0px;
	top: 0px;
	
	background-color:#ffffff;
	-webkit-clip-path: polygon(440px 0%, 100% 0%, 100% 100%, 0% 100%);
}

.shape-c3-img{
	height: 100%;
	width: 100%;
	position:absolute;
	right:0px;
	top: 0px;
		
	background-image:url(<?=$img2?>);	
	background-size:cover;
	background-position:center;
}

.shape-c3-img,
.shape-c3-img:after{
	-webkit-clip-path: polygon(
	calc(440px + 20px) 0%, 
	100% 0%, 
	100% 100%, 
	calc(0% + 20px) 100%);
}

/* Rote Abdunklung */
.shape-c3-img:after{
	content:"";
	position:absolute;
	top:0px;
	width:100%;
	height:100%;
	background-color:rgba(255,55,0,.6);
}





/* ------------------------------------------------- */
/* Title und Beschriftungen */
.imgtitle3,
.maintitle3{
	line-height: 1;
}
	

.imgtitle3{
	position: absolute;
    bottom: -100px;
    font-size: 40px;
    text-transform: uppercase;
    color: #ffffff;
    z-index: 20;
    width: 30%;
    right: -2%;
	font-weight:bold;
}

.maintitle3{
	position: absolute;
    bottom: -220px;
    font-size: 40px;
    text-transform: uppercase;
    color: #000000;
    z-index: 20;
    width: 33%;
    left: 0%;
    border-top: 5px solid #000000;
    font-weight: bold;
    text-align: left;
}







/* ------------------------------------------------- */
/* Responsive Darstellung */
@media screen and (max-width:1000px)
{
	.shape-a3-img{
		display:none;
	}
	
	.shape-c3,
	.shape-c3-img,
	.shape-c3-img:after
	{
		-webkit-clip-path: polygon(0% 0%, 100% 0%, 100% 100%, 0% 100%);
	}
	
	.outer-header,
	.shape-c3,
	.shape-c3-img,
	.shape-c3:after{
		height:200px;
		width:100%;
	}
	
	.imgtitle3{
		font-size:30px;
		width:100%;
		bottom:20px;
		left:0px;
		text-align:center;
		padding:20px;
		box-sizing:border-box;
	}

	.maintitle3{
		left:0%;
		padding:20px;
		width:80%;
	}
}

</style>




<div class="outer-header">

	<div class="shape-a3-img">
	</div>

	<div class="inner-header">
		<div class="imgtitle3">
			<?=$title?>
		</div>

		<div class="maintitle3">
			<?=$subtitle?>
		</div>
	</div>
	
	<div class="shape-c3">
		<div class="shape-c3-img">
		</div>
	</div>
</div>
