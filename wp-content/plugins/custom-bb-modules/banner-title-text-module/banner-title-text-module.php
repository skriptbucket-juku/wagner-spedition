<?php

/**
 * @class BannerTitleTextModule
 */
class BannerTitleTextModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('BannerTitleTextModule', 'fl-builder'),
			'description'   	=> __('Banner mit vier Bildboxen (1. Box rot)', 'fl-builder'),
			'category'      	=> __('Wagner-Module', 'fl-builder'),
			'partial_refresh'	=> true
		));

		$this->add_css('jquery-bxslider');
		$this->add_js('jquery-bxslider');
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('BannerTitleTextModule', array(
	'general'       => array(
		'title'         => __('General', 'fl-builder'),
		'sections'      => array(
			'general'       => array(
				'title'         => '',
				'fields'        => array(
				
				

					'title'          => array(
						'type'          => 'editor',
						'label'         => 'Titel',
						'rows'          => 2,
						'wpautop'		=> false,
						'preview'         => array(
							'type'             => 'text',
							'selector'         => '.fl-rich-text',
						),
						'connections'   => array( 'string' ),
					),		

					'description'          => array(
						'type'          => 'editor',
						'label'         => 'Beschreibung',
						'rows'          => 2,
						'wpautop'		=> false,
						'preview'         => array(
							'type'             => 'text',
							'selector'         => '.fl-rich-text',
						),
						'connections'   => array( 'string' ),
					),						
					
					
			
				)
			)
		)
	)
));