(function($) {
	<?php
		if (count($settings->textes) > 0)
		{
	?>
			var textes = $('.fl-node-<?php echo $id; ?> .imageBoxWidget .imageBoxWidgetText'),
			fadeTime = 2000,   // FADE TIME
			delay = <?php echo $settings->delay * 1000; ?> + fadeTime,  // PAUSE TIME
			index = -1;    // START textes index (will turn 0 after initial kick)

			function nextText()
			{
				textes.eq(++index%textes.length).fadeTo(fadeTime, 1).siblings(textes).stop(1).fadeTo(fadeTime, 0);
			}
			nextText();

			setInterval(nextText, delay);
	<?php
		}
	?>
})(jQuery);