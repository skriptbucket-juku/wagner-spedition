<?php

/**
 * Plugin Name: BB Custom Modules
 * Plugin URI: https://www.skriptstube.de
 * Description: Custom modules for the Beaver Builder Plugin.
 * Version: 1.0
 * Author: skriptstube GmbH
 * Author URI: https://www.skriptstube.de
 */
define( 'MY_MODULES_DIR', plugin_dir_path( __FILE__ ) );
define( 'MY_MODULES_URL', plugins_url( '/', __FILE__ ) );

function load_modules() {
    if ( class_exists( 'FLBuilder' ) ) {
        require_once 'stellenanzeige-module/stellenanzeige-module.php';
		require_once 'management-module/management-module.php';
		require_once 'event-module/event-module.php';
		require_once 'head-three-module/head-three-module.php';
		require_once 'banner-red-normal-module/banner-red-normal-module.php';
		require_once 'banner-four-boxes-module/banner-four-boxes-module.php';
		require_once 'banner-title-text-module/banner-title-text-module.php';
		require_once 'banner-video-module/banner-video-module.php';
		require_once 'blog-like-posts-module/blog-like-posts-module.php';
		require_once 'head-no2-module/head-no2-module.php';
		require_once 'head-no3-module/head-no3-module.php';
		require_once 'banner-normal-red-module/banner-normal-red-module.php';
		require_once 'blog-module/blog-module.php';
		require_once 'histroy-module/histroy-module.php';
		
    }
}
add_action( 'init', 'load_modules' );

?>