<?php global $wp_embed; ?>
<style>
.pdfdownload{
	text-transform: uppercase;
	border: 1px solid #4f9fa6;
	border-radius:6px;
	padding:10px 15px;
	margin:0px 0px 15px 0px;
	display:inline-block;
	cursor:pointer;
}

.pdfdownload:after{
	font-family:"FontAwesome";
	content:"\f019";
	margin-left:10px;
}

.eventview.event{
	overflow:hidden;
	display:table; 
}

.eventview.row{
	display: table-row;
}

.eventview.inside{
	padding:20px;
	/*float:left;	*/
	display: table-cell; 
}

.eventview.pic{
	width:45%;
} 

.eventview.desc{
	width:55%;
	vertical-align:middle;
} 

.eventview.desc .event_title{
	text-transform:uppercase;
	font-weight:bold;
}

.eventview.desc .event_date{
	text-transform:uppercase;
	color:#4f9fa6;
	font-size:70%;
	margin-bottom:30px;
}

.eventview.desc .event_desc{
	font-size:80%;
}

.green_hr{
	height:1px;
	background-color:#4f9fa6;
	margin:10px 0px;
}

</style>


<?php 
for ( $i = 0; $i < count( $settings->events ); $i++)
{
	// Grüne Linie
	echo('<div class="green_hr"></div>');
	
	if($i%2 == 0)
	{
	?>
	<div class="eventview event">
		<div class="eventview row">
			<div class="eventview pic inside">
				<?php
				if($settings->events[ $i ]->photo_src != "")
				{
					echo("<img src='".$settings->events[ $i ]->photo_src."' />");
				}
				?>
			</div>
			<div class="eventview desc inside">
				<div class="verticalmiddle">
					<p class="event_title"><?=$settings->events[ $i ]->title?></p>
					<p class="event_date"><?=$settings->events[ $i ]->datefield?></p>
					<div class="event_desc"><?=( $wp_embed->autoembed( $settings->events[ $i ]->description ) ); ?></div>
				</div>
			</div>
		</div>
	</div>
	<div style="clear:both"></div>
	<?php
	}
	else
	{
		?>
	<div class="eventview event">
		<div class="eventview row">
			<div class="eventview desc inside">
				<div class="verticalmiddle">
					<p class="event_title"><?=$settings->events[ $i ]->title?></p>
					<p class="event_date"><?=$settings->events[ $i ]->datefield?></p>
					<div class="event_desc"><?=( $wp_embed->autoembed( $settings->events[ $i ]->description ) ); ?></div>
				</div>
			</div>
			<div class="eventview pic inside">
				<?php
				if($settings->events[ $i ]->photo_src != "")
				{
					echo("<img src='".$settings->events[ $i ]->photo_src."' />");
				}
				?>
			</div>
		</div>
	</div>
	<div style="clear:both"></div>
	<?php
	}
}
if(count( $settings->events ) > 0)
{
	// Grüne Linie
	echo('<div class="green_hr"></div>');
}


?>
