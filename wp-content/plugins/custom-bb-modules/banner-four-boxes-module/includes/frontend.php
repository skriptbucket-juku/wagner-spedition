<?php

$fullsize_path_img1 = basename(get_attached_file($settings->photo));
$fullsize_path_img2 = basename(get_attached_file($settings->photo2));
$fullsize_path_img3 = basename(get_attached_file($settings->photo3));
$fullsize_path_img4 = basename(get_attached_file($settings->photo4));

$img1 = "/wp-content/uploads/" . $fullsize_path_img1;
$img2 = "/wp-content/uploads/" . $fullsize_path_img2;
$img3 = "/wp-content/uploads/" . $fullsize_path_img3;
$img4 = "/wp-content/uploads/" . $fullsize_path_img4;

$title = $settings->title;
?>
<style>

.card-spike{
	height:500px;
	width:calc(100% - 10px);
	background-color:#ffffff;
	-webkit-clip-path: polygon(0% 0%, 100% 0%, 100% 90%, 30% 90%, 20% 100%, 10% 90%, 0% 90%);
	position:absolute;
	top:0px;
	background-size:cover;
	background-position:center;
	z-index:10;
}

.card-spike:after{
	content:"";
	display:block;
	height:100%;
	width:100%;
	background-color:rgba(255,55,0,.5);
	-webkit-clip-path: polygon(0% 0%, 100% 0%, 100% 90%, 30% 90%, 20% 100%, 10% 90%, 0% 90%);
	position:absolute;
	top:0px;
}

.card-nospike{
	height:500px;
	width:calc(100% - 10px);
	background-color:#ffffff;
	-webkit-clip-path: polygon(0% 0%, 100% 0%, 100% 90%, 0% 90%);
	position:absolute;
	top:0px;
	background-size:cover;
	background-position:center;
}

.card-1{
	background-image:url(<?=$img1?>);	
}


.card-2{
	background-image:url(<?=$img2?>);	
}


.card-3{
	background-image:url(<?=$img3?>);	
}


.card-4{
	background-image:url(<?=$img4?>);	
}

.card-spike,
.card-nospike{
	margin-left:5px;
}

.h-100{
	height:100%;
}

.card-title{
	position:absolute;
	top:30%;
	z-index:20;
	padding:10%;
	color:#ffffff;
}

@media screen and (max-width:767px)
{
	.card-nospike{
		display:none;
	}
}

</style>


<div class="row mx-0 my-5" style="height:500px;">
	<div class="col-md-3 p-0 h-100">
		<div class="card-title">
			<?=$title?>
		</div>
		<div class="card-spike card-1">
		</div>
	</div>
	<div class="col-md-3 p-0 h-100">
		<div class="card-nospike card-2">
		</div>
	</div>
	<div class="col-md-3 p-0 h-100">
		<div class="card-nospike card-3">
		</div>
	</div>
	<div class="col-md-3 p-0 h-100">
		<div class="card-nospike card-4">
		</div>
	</div>
</div>

